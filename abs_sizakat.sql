-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2015 at 10:53 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `abs_sizakat`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(8) NOT NULL,
  `NamaDonatur` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `username`, `password`, `role`, `NamaDonatur`) VALUES
(1, 'admin', 'abssizakat', 'admin', 'admin'),
(2, 'testing', '12345', 'donatur', 'PKPU'),
(3, 'coba2', '12345', 'donatur', 'PKPU'),
(4, 'coba1', '12345', 'donatur', 'PKPU'),
(5, 'cimb3', '123456', 'donatur', 'CIMB Niaga Syariah');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `idprogram` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `biaya` int(11) NOT NULL,
  `donatur` varchar(200) NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'Belum Berjalan',
  `tanggalmulai` date NOT NULL,
  `tanggalakhir` date NOT NULL,
  `foto` varchar(100) NOT NULL,
  `ActualProgress` int(5) NOT NULL,
  `asnaf` varchar(50) DEFAULT NULL,
  `penanggungjawab` varchar(50) NOT NULL,
  `tujuan` text NOT NULL,
  `lokasi` text NOT NULL,
  PRIMARY KEY (`idprogram`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=139 ;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`idprogram`, `nama`, `biaya`, `donatur`, `status`, `tanggalmulai`, `tanggalakhir`, `foto`, `ActualProgress`, `asnaf`, `penanggungjawab`, `tujuan`, `lokasi`) VALUES
(106, 'PEMBANGUNAN MASJID MARYAM AHMAD JUMAH', 100000000, 'HAI UEA', 'Sedang Berjalan', '2014-01-07', '2014-01-07', 'images/1.jpg', 80, 'fakirmiskin,,,,,,,', 'Abdul Syakur', 'Pendirian masjid baru/masjid makmur dg Jamaah banyak min 50 orang dg Ust Pembimbing', 'Perumahan kirana cibitung, rt7/19. cibitung, bekasi, jawa barat, cp teguh pkpu 021 44467409'),
(107, 'INDEPENDENT CART', 500000000, 'HAI Australia', 'Selesai', '2014-01-07', '2014-02-28', 'images/107.jpg', 101, 'fakirmiskin,,,,,,,', 'mukhlis', 'untuk membiayai program micro finance - gerobak untuk 1000 pm agar mandiri', 'Jabodetabek'),
(108, 'House Renovation', 642000000, 'HAI Australia', 'Selesai', '2014-01-07', '2014-04-07', 'images/108.jpg', 50, 'fakirmiskin,,,,,,,', 'Abdul Syakur', 'membiayai 50 rumah ', 'Tenjo, Bogor dan Indramayu (35 unit). PKPU Cab Jogja, Balikpapan, Surabaya, Semarang, Maluku (15 unit)'),
(109, 'Mosque and Imam''s House Construction Project', 263109162, 'HAI AUSTRALIA', 'Sedang Berjalan', '2014-01-10', '2014-06-24', 'images/109.jpg', 80, ',amil,,,,,,', 'Abdul Syakur', 'membiayai 50 rumah ', 'Dusun Sidorukun Desa Clumprit RT.18/03 Kecamatan Pagelangan Kab. Malang - Jawa Timur'),
(110, 'Aqiqah a/n Khansa Fadheela Harahap binti Kennet Faisal Harahap', 1500000, 'Kennet Faisal Harahap & Siti Nurfalah', 'Selesai', '2014-01-10', '2014-01-10', 'images/110.jpg', 100, 'fakirmiskin,,,,,,,', 'Bramadji', 'Membiayai Aqiqah', 'Yayasan Ummul Yatama Perum Cluster Pesona Lorena Blok B18 Babakan Mustika Sari Kel. Mustika Jaya Bekasi - Jawa Barat'),
(111, 'Kafalah Ustadz', 5000000, 'Yayasan Baitul Hikmah ', 'Selesai', '2014-01-08', '2014-01-15', 'images/111.jpg', 90, 'fakirmiskin,,,,,,,', 'Bramadji', 'Membantu korban bencana Gunung Sinabung', 'Pengungsian Gunung Sinabung, Tana Karo-Medan'),
(112, 'Prosmiling (Pengobatan Umum)', 4015000, 'DKM Al Hidayah Perum Villa Nusa Indah 2', 'Selesai', '2014-01-19', '2014-01-19', 'images/112.jpg', 100, 'fakirmiskin,,,,,,,', 'Afrizal', 'EMBANTU MASYARAKAT DHUAFA DI SEKITAR VILA NUSA INDAH 2, ', 'Masjid Al Hidayah, Villa Nusa Indah 2 Desa Bojong Kulur Kec Gunung Putri Bogor'),
(114, 'Pemberian Peralatan Rumah ke Yatim HAI Emirat', 684000000, 'HAI UEA', 'Selesai', '2014-01-15', '2014-01-30', 'images/114.jpg', 100, ',,,,,,ibnusabil', 'Abdul Syakur', 'Tersedianya Kebutuhan Peralatan Rumah Yatim Sehari hari. Pemberian Kompor Gas ($ 33) dan alat pendingin/kipas Angin ($ 24)', 'Rumah Yatim masing masing'),
(115, 'Prosmiling Layanan Kesehatan Korban Banjir 2014', 40000000, 'PT. Bank CIMB Niaga, Tbk', 'Selesai', '2014-01-17', '2014-01-31', 'images/115.jpg', 100, 'fakirmiskin,,,,,,,', 'Afrizal', 'Pelayanan kesehatan bagi para korban banjir Jakarta Januari 2014', 'Silahkan di survei (bebas)'),
(116, 'Prosmiling Layanan Kesehatan Korban Banjir 2015', 250000000, 'PT. Indosat, Tbk', 'Selesai', '2014-01-20', '2014-01-21', 'images/116.jpg', 100, 'fakirmiskin,,,,,,,', 'Afrizal', 'Membagikan paket logistik sesuai dengan anggaran yang terlampir', 'Bekasi : Muara Gembong, Babelan & Tambun. Karawang : Jatiragas, Jatisari, Desa Cibeet karawang dan Desa Bobojong.'),
(119, 'Pembangunan Fasilitas Umum Jamban (Program 1000 jamban )', 30000000, 'HS Rudi', 'Sedang Berjalan', '2014-02-13', '2014-05-14', 'images/119.jpg', 3, 'fakirmiskin,,,,,,,', 'Siti Maesaroh', 'Pembangunan 2 jamban (tipe disesuaikan) ', 'Kp Bahari ds Sidamukti RT04 RW01 kecamatan Sukaresmi Pandeglang Banten (Pic wilayah 081807490657 Ahmad )'),
(120, 'Paket Logistik', 15000000, 'ZIS INDOSAT', 'Selesai', '2014-02-09', '2014-02-09', 'images/120.jpg', 100, 'fakirmiskin,,,,,,,', 'Afrizal', 'Membantu Warga Kp. Muara Baru', 'Kp. Muara Baru'),
(121, 'Perpustakaan Keliling Tabung Peduli', 3420125, 'Mitra Tabung Peduli', 'Belum Berjalan', '2014-05-05', '2014-05-31', 'images/121.jpg', 0, ',,,,,,,', 'Abdul Syakur', 'Menjalankan program sesuai RKAT', 'daerah korban letusan gunung Kelud, Jawa Timur'),
(123, 'Yatim IDB', 400000000, 'PKPU', 'Belum Berjalan', '2014-04-11', '2014-04-30', 'images/123.jpg', 0, ',,,,,,,', 'Abdul Syakur', 'Menjalankan program sesuai RKAT', 'Indonesia'),
(125, 'Indonesia Peduli (Air Untuk Sinabung)', 173406741, 'PT Aplikanusa Lintasarta', 'Selesai', '2014-04-11', '2014-04-30', 'images/125.jpg', 100, ',,,,,,,', 'Afrizal', 'Memberikan sarana air bersih yang layak untuk para korban sinabung dan warga sekitar wilayah yang telah disepakati', 'Masjid At Taqwa, Desa Kinnepen, kecamatan Munte, Kabupaten Karo Sumatera Utara'),
(126, 'PROSMILING', 7500000, 'PPDI PT. JICT', 'Selesai', '2014-03-23', '2014-03-23', 'images/126.jpg', 40, ',,,,,,,', 'Afrizal', 'memberikan layanan kesehatan bagi masyarakat', 'Kampung Jembatan, Klender, Jakarta Timur'),
(128, 'PROSMILING (UMUM)', 6000000, 'DKM BAABUT TAUBAH PT. DENSO INDONESIA', 'Selesai', '2014-04-07', '2014-04-15', 'images/128.jpg', 0, 'fakirmiskin,,,,,,,', 'Afrizal', 'Pelayanan kesehatan untuk korban erupsi gunung kelud', 'Sekitar gunung Kelud, Jawa timur.'),
(129, 'PEMBUATAN SUMUR WATSAN SUMUR GALIH / POMPA TANGAN HJ INTISOR ABDURRAHMAN DAWUD', 6501000, 'HAI EMIRAT', 'Belum Berjalan', '2014-06-05', '2014-06-06', 'images/129.jpg', 0, ',,,,,,,', 'Vina Anggraini', 'Penyediaan sumber air bersih di daerah yg memerlukan / sulit air', 'Indonesia'),
(130, 'PEMBANGUNAN MASJID MAUZAH SOLIH ALI SALIM AS SIBLI', 251417600, 'HAI UEA', 'Sedang Berjalan', '2014-04-15', '2014-08-13', 'images/130.jpg', 10, ',,,,,,,', 'Bramadji', 'Pendirian masjid baru/masjid makmur dg Jamaah banyak min 200 orang dg Ust Pembimbing', 'YAYASAN ANWARUL QULUB-DESA PABUARAN RT03/02 KEC BOJONG GEDE, BOGOR'),
(131, 'Donasi Bantuan Kelud Benah Madrasah Minor', 50000000, 'CIMB Niaga Syariah', 'Sedang Berjalan', '2014-04-23', '2014-05-14', 'images/131.jpg', 10, ',,,,,,,', 'Abdul Syakur', 'Perbaikan Madrasah yang terkena dampak erupsi Kelud', 'Kediri/Blitar/Malang');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
