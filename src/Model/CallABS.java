package Model;

import abs.backend.java.lib.runtime.ABSRuntime;
import abs.backend.java.lib.runtime.COG;
import ModABSQuery.Main;


public class CallABS {

	public String setSelectABS(String attr, String table,String con) throws Exception{
		ABSRuntime runtime = new ABSRuntime();
        COG cog = runtime.createCOG(Main.class);
		Main m = new Main(cog);
		m.setSelect(attr,table,con);
		while(m.select == false)
		{
			Thread.yield();
		}
		runtime.start(Main.class);
		String hore = m.temperesult;
		while(hore == "")
		{
			Thread.yield();
		}
		return hore;
	}
	
	public String setUpdateABS(String attr, String table,String con) throws Exception{
		ABSRuntime runtime = new ABSRuntime();
        COG cog = runtime.createCOG(Main.class);
		Main m = new Main(cog);
		m.setUpdate(attr, table, con);
		while(m.update == false)
		{
			Thread.yield();
		}
		runtime.start(Main.class);
		while( m.result == "")
		{
			Thread.yield();
		}
		return  m.result;
	}
	
	public static String setDeleteABS(String table, String constraint) throws Exception{
		ABSRuntime runtime = new ABSRuntime();
        COG cog = runtime.createCOG(Main.class);
		Main m = new Main(cog);
		m.setDelete(table, constraint);
		while(Main.delete == false)
		{
			Thread.yield();
		}
		runtime.start(Main.class);
		while(m.result == "")
		{
			Thread.yield();
		}
		return m.result;
	}
	
	public String setInsertABS(String attr, String table,String values) throws Exception{
		ABSRuntime runtime = new ABSRuntime();
        COG cog = runtime.createCOG(Main.class);
		Main m = new Main(cog);
		m.setInsert(attr, table, values);
		while(m.insert == false)
		{
			Thread.yield();
		}
		runtime.start(Main.class);
		String hore = m.temperesult;
		while(hore == "")
		{
			Thread.yield();
		}
		return hore;
	}
}

