package Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import java.util.Properties;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class ProgramDAO {
	static Connection currentCon = null;
	ConnectionManager con = null;
	static ResultSet rs = null;
	static Properties props;
	String propertiesFileName = "config.properties";
	Properties properti = null;

	public ProgramDAO() throws IOException {
		con = new ConnectionManager();
		currentCon = con.getConnection();
		properti = new Properties();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);
		properti.load(inputStream);
	}

	public boolean create(Program pgm) throws Exception {

		Statement st = null;
		ResultSet rs = null;

		boolean isSucces = false;
		String name = pgm.getNama();
		int biaya = pgm.getBiaya();
		String donatur = pgm.getDonatur();
		String status = pgm.getStatus();
		Date tanggalMulai = pgm.getTanggalMulai();
		Date tanggalAkhir = pgm.getTanggalAkhir();
		InputStream foto = pgm.getFoto();
		int progres = pgm.getProgress();
		String asnaf = pgm.getAsnaf();
		String penanggungjawab = pgm.getPenanggungjawab();
		String tujuan = pgm.getTujuan();
		String lokasi = pgm.getLokasi();

		String fotoNama = "";
		String databaseFotoPath = "";

		int lastid = 0;
		st = currentCon.createStatement();
		rs = st.executeQuery("SELECT max( idprogram ) AS last_id FROM program");
		boolean exist = rs.next();

		if (exist) {
			lastid = rs.getInt("last_id") + 1;

			fotoNama = properti.getProperty("imgdir") + lastid + ".jpg";
			databaseFotoPath = properti.getProperty("imgdir2") + lastid + ".jpg";
			System.out.println(fotoNama);

		}
		st.close();

		PreparedStatement psmt = null;

		String query = "insert into program values(?,?,?,?,?,?,?,?,?,?,?,?,?)";

		psmt = currentCon.prepareStatement(query);

		psmt.setString(1, null);
		psmt.setString(2, name);
		psmt.setInt(3, biaya);
		psmt.setString(4, donatur);
		psmt.setString(5, status);
		psmt.setDate(6, tanggalMulai);
		psmt.setDate(7, tanggalAkhir);
		psmt.setString(8, databaseFotoPath);
		psmt.setInt(9, progres);
		psmt.setString(10, asnaf);
		psmt.setString(11, penanggungjawab);
		psmt.setString(12, tujuan);
		psmt.setString(13, lokasi);

		int rowsUpdate = psmt.executeUpdate();

		if (rowsUpdate > 0) {

			OutputStream outputStream = new FileOutputStream(new File(fotoNama));

			int read = 0;
			byte[] bytes = new byte[2048];

			while ((read = foto.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			if (foto != null) {
				try {
					// outputStream.flush();
					foto.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			isSucces = true;
		} else {
			System.out.println("GAK MASUK WOI");
		}
		return isSucces;

	}

	public boolean update(Program pgm) throws SQLException, IOException {

		boolean isSucces = false;
		int id = pgm.getId();
		String name = pgm.getNama();
		int biaya = pgm.getBiaya();
		String donatur = pgm.getDonatur();
		String status = pgm.getStatus();
		Date tanggalMulai = pgm.getTanggalMulai();
		Date tanggalAkhir = pgm.getTanggalAkhir();
		InputStream foto = pgm.getFoto();
		int progres = pgm.getProgress();
		String asnaf = pgm.getAsnaf();
		String penanggungjawab = pgm.getPenanggungjawab();
		String tujuan = pgm.getTujuan();
		String lokasi = pgm.getLokasi();

		String fotoNama = properti.getProperty("imgdir") + id + ".jpg";
		String databaseFotoPath = properti.getProperty("imgdir2") + id + ".jpg";

		PreparedStatement psmt = null;

		String query = "update program set nama = ?, biaya = ?, donatur = ?, status = ?, tanggalmulai = ?, tanggalakhir = ?,foto=?, actualProgress=?,asnaf = ?, penanggungjawab = ?, tujuan = ?, lokasi = ? where idprogram = ?";

		psmt = currentCon.prepareStatement(query);

		psmt.setString(1, name);
		psmt.setInt(2, biaya);
		psmt.setString(3, donatur);
		psmt.setString(4, status);
		psmt.setDate(5, tanggalMulai);
		psmt.setDate(6, tanggalAkhir);
		psmt.setString(7, databaseFotoPath);
		psmt.setInt(8, progres);
		psmt.setString(9, asnaf);
		psmt.setString(10, penanggungjawab);
		psmt.setString(11, tujuan);
		psmt.setString(12, lokasi);
		psmt.setInt(13, id);

		int rowsUpdate = psmt.executeUpdate();

		if (rowsUpdate > 0) {
			OutputStream outputStream = new FileOutputStream(new File(fotoNama));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = foto.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			if (foto != null) {
				try {
					// outputStream.flush();
					foto.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			isSucces = true;
		} else {
			System.out.println("GAK MASUK WOI");
		}
		return isSucces;
	}

	public boolean updateDonatur(String id, String daftarBaru) throws SQLException, IOException {
		boolean isSucces = false;

		Statement stmt = null;
		String query = "UPDATE program SET donatur='" + daftarBaru + "' WHERE idprogram = '" + id + "'";

		stmt = currentCon.createStatement();
		int update = stmt.executeUpdate(query);
		if (update > 0) {
			isSucces = true;
		}

		return isSucces;
	}

	public boolean delete(String id) throws SQLException {

		boolean isSucces = false;

		Statement stmt = null;
		String query = "DELETE FROM program where idprogram = " + id;

		stmt = currentCon.createStatement();
		int rowsUpdate = stmt.executeUpdate(query);
		if (rowsUpdate > 0) {
			File f = new File(properti.getProperty("imgdir") + id + ".jpg");

			Boolean flag = false;
			if (f.exists()) {
				flag = f.delete();
			}

			isSucces = true;
		}

		return isSucces;

	}

	public Program getProgram(String id) throws Exception {

		Statement stmt = null;
		ResultSet rs = null;

		Program pgm = new Program();

		String query = "Select * from program where idprogram = " + id;
		stmt = currentCon.createStatement();

		rs = stmt.executeQuery(query);
		boolean exist = rs.next();

		if (exist) {
			pgm = Program.load(rs);
		}
		stmt.close();

		return pgm;
	}

	public List<Program> gettopprogram() throws Exception {
		List<Program> list = new ArrayList<Program>();
		ResultSet rs = null;
		Statement stmt = null;

		String query = "SELECT *FROM (SELECT * FROM program ORDER BY idprogram DESC ) AS p LIMIT 5 ";
		stmt = currentCon.createStatement();

		rs = stmt.executeQuery(query);

		while (rs.next()) {
			Program pgm = new Program();

			pgm = Program.load(rs);
			list.add(pgm);
		}

		if (rs != null)
			rs.close();

		stmt.close();
		return list;

	}

	public List<Program> selectAllProgram() throws Exception {
		List<Program> list = new ArrayList<Program>();
		ResultSet rs = null;
		Statement stmt = null;

		String query = "Select * from program order by idprogram desc";
		stmt = currentCon.createStatement();

		rs = stmt.executeQuery(query);

		while (rs.next()) {
			Program pgm = new Program();
			// set fields of c
			pgm = Program.load(rs);
			list.add(pgm);
		}

		if (rs != null)
			rs.close();

		stmt.close();
		return list;

	}

	public List<Program> selectSomeProgram(int offset, int noOfRecords) throws Exception {
		List<Program> list = new ArrayList<Program>();
		ResultSet rs = null;
		Statement stmt = null;

		String query = "Select * from program order by idprogram desc limit " + offset + ", " + noOfRecords;
		stmt = currentCon.createStatement();

		rs = stmt.executeQuery(query);

		while (rs.next()) {
			Program pgm = new Program();
			// set fields of c
			pgm = Program.load(rs);
			list.add(pgm);
		}

		if (rs != null)
			rs.close();

		stmt.close();
		return list;

	}

	public List<Program> selectProgramDonatur(String donaturName) throws Exception {
		List<Program> list = new ArrayList<Program>();
		ResultSet rs = null;
		Statement stmt = null;

		String query = "(Select * from program where donatur = '" + donaturName + "')"
				+ " union (Select * from program where donatur like '" + donaturName + "-%')"
				+ " union (Select * from program where donatur like '%-" + donaturName + "-%')"
				+ " union (Select * from program where donatur like '%-" + donaturName + "')";
		stmt = currentCon.createStatement();

		rs = stmt.executeQuery(query);

		while (rs.next()) {
			Program pgm = new Program();
			// set fields of c
			pgm = Program.load(rs);
			list.add(pgm);
		}

		if (rs != null)
			rs.close();

		stmt.close();
		return list;

	}

	public static void downloadExcel(String path) throws SQLException, IOException {
		// PrintStream p = new PrintStream(new FileOutputStream(
		// "E:/error.txt"));
		// System.setOut(p);
		PreparedStatement stmt = null;
		ResultSet rs = null;

		// New Workbook
		Workbook wb = new XSSFWorkbook();

		Cell c = null;

		// Cell style for header row
		CellStyle cs = wb.createCellStyle();
		cs.setFillForegroundColor(IndexedColors.LIME.getIndex());
		cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
		Font f = wb.createFont();
		f.setBoldweight(Font.BOLDWEIGHT_BOLD);
		f.setFontHeightInPoints((short) 12);
		cs.setFont(f);

		// New Sheet
		Sheet sheet1 = null;
		sheet1 = wb.createSheet("myData");

		String query = "Select * from program";

		stmt = currentCon.prepareStatement(query);
		rs = stmt.executeQuery();

		ResultSetMetaData metaData = rs.getMetaData();
		int colCount = metaData.getColumnCount();

		// Create Hash Map of Field Definitions
		LinkedHashMap<Integer, TableInfo> hashMap = new LinkedHashMap<Integer, TableInfo>(colCount);

		for (int i = 0; i < colCount; i++) {
			TableInfo db2TableInfo = new TableInfo();
			db2TableInfo.setFieldName(metaData.getColumnName(i + 1).trim());
			db2TableInfo.setFieldText(metaData.getColumnLabel(i + 1));
			db2TableInfo.setFieldSize(metaData.getPrecision(i + 1));
			db2TableInfo.setFieldDecimal(metaData.getScale(i + 1));
			db2TableInfo.setFieldType(metaData.getColumnType(i + 1));
			db2TableInfo.setCellStyle(getCellAttributes(wb, c, db2TableInfo));
			hashMap.put(i, db2TableInfo);
		}

		// Row and column indexes
		int idx = 0;
		int idy = 0;

		// Generate column headings
		Row row = sheet1.createRow(idx);
		TableInfo db2TableInfo = new TableInfo();

		Iterator<Integer> iterator = hashMap.keySet().iterator();
		while (iterator.hasNext()) {
			Integer key = iterator.next();
			db2TableInfo = hashMap.get(key);
			c = row.createCell(idy);
			c.setCellValue(db2TableInfo.getFieldText());
			c.setCellStyle(cs);
			int width = 0;
			if (db2TableInfo.getFieldSize() > 100) {
				// width = db2TableInfo.getFieldSize() * 100
				width = 5000;
				// System.out.println(db2TableInfo.getFieldSize());
			} else {
				width = 5000;
			}
			if (db2TableInfo.getFieldSize() > db2TableInfo.getFieldText().trim().length()) {
				// p.append(""+width);
				sheet1.setColumnWidth(idy, width);
			} else {
				sheet1.setColumnWidth(idy, width);
			}
			idy++;
		}

		while (rs.next()) {

			idx++;
			row = sheet1.createRow(idx);
			for (int i = 0; i < colCount; i++) {

				c = row.createCell(i);
				db2TableInfo = hashMap.get(i);

				switch (db2TableInfo.getFieldType()) {
				case 1:
					c.setCellValue(rs.getString(i + 1));
					break;
				case 2:
					c.setCellValue(rs.getDouble(i + 1));
					break;
				case 3:
					c.setCellValue(rs.getDouble(i + 1));
					break;
				default:
					c.setCellValue(rs.getString(i + 1));
					break;
				}
				c.setCellStyle(db2TableInfo.getCellStyle());
			}

		}

		rs.close();
		stmt.close();
		currentCon.close();

		FileOutputStream fileOut = new FileOutputStream(path);

		wb.write(fileOut);
		fileOut.close();

	}

	private static CellStyle getCellAttributes(Workbook wb, Cell c, TableInfo db2TableInfo) {

		CellStyle cs = wb.createCellStyle();
		DataFormat df = wb.createDataFormat();
		Font f = wb.createFont();

		switch (db2TableInfo.getFieldDecimal()) {
		case 1:
			cs.setDataFormat(df.getFormat("#,##0.0"));
			break;
		case 2:
			cs.setDataFormat(df.getFormat("#,##0.00"));
			break;
		case 3:
			cs.setDataFormat(df.getFormat("#,##0.000"));
			break;
		case 4:
			cs.setDataFormat(df.getFormat("#,##0.0000"));
			break;
		case 5:
			cs.setDataFormat(df.getFormat("#,##0.00000"));
			break;
		default:
			break;
		}

		cs.setFont(f);

		return cs;

	}

	public HashMap<String, Object> upload(InputStream fileInputStream) throws InvalidFormatException{
		HashMap<String, Object> result = new HashMap<>();
		ArrayList<String> unsuccessResult = new ArrayList<String>();
		int successCounter = 0;
		
		try{

			String sql = null;
			boolean hasil = false;
			PreparedStatement stmt = null;

			sql = "SELECT * from program";
			stmt = currentCon.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			ResultSetMetaData metaData = rs.getMetaData();
			int colCount = metaData.getColumnCount();

			String q = "";
			String name = "";
			if (colCount < 13) {
				hasil = false;
			} else {
				for (int c = 0; c < colCount; c++) {
					if (q.equalsIgnoreCase("")) {
						q = "?";
						name = metaData.getColumnName(c + 1);
					} else {
						q = q + ",?";
						name = name + "," + metaData.getColumnName(c + 1);
					}
				}
				sql = "INSERT into program" + " (" + name + ") VALUES(" + q + ")";
				System.out.println(sql);
				stmt.close();
				stmt = currentCon.prepareStatement(sql);
				boolean insertData = true;

				XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
				XSSFSheet sheet = workbook.getSheetAt(0);

				int rows = sheet.getPhysicalNumberOfRows() + 5;

				System.out.println("rows : " + rows);
				for (int r = 0; r < rows; r++) {

					XSSFRow row = sheet.getRow(r);
					if (row == null) {
						continue;
					}

					if (r == 0) {
						continue;
					}

					int cells = row.getPhysicalNumberOfCells();

					System.out.println("cells : " + cells);
					
					for (int c = 0; c < cells; c++) {
						XSSFCell cell = row.getCell(c);
						String value = "";

						if (cell == null || c == 0) {
							stmt.setString(cell.getColumnIndex() + 1, null);
							continue;
						}

						switch (cell.getCellType()) {

						case XSSFCell.CELL_TYPE_FORMULA:
							System.out.println("cell tytpe : formula");

							value = cell.getCellFormula();
							stmt.setString(cell.getColumnIndex() + 1, value);
							break;

						case XSSFCell.CELL_TYPE_NUMERIC:
							System.out.println("cell tytpe : numeric");
							System.out.println(cell.getNumericCellValue());
							value = String.valueOf(cell.getNumericCellValue());
							stmt.setInt(cell.getColumnIndex() + 1, (int) Double.parseDouble(value));
							break;

						case XSSFCell.CELL_TYPE_STRING:
							System.out.println("cell tytpe : string");

							value = cell.getStringCellValue();
							System.out.println(cell.getStringCellValue());

							if (isValidDate(value)) {
								try {
									java.util.Date tgl = new SimpleDateFormat("dd/MM/yyyy").parse(value);
									Date date = new java.sql.Date(tgl.getTime());

									stmt.setDate(cell.getColumnIndex() + 1, date);
								} catch (ParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									hasil = false;
								}
							} else {
								stmt.setString(cell.getColumnIndex() + 1, value);
							}

							break;

						case XSSFCell.CELL_TYPE_BLANK:
							System.out.println("cell tytpe : blank");
							value = cell.getStringCellValue();
							stmt.setString(cell.getColumnIndex() + 1, null);
							break;

						default:
						}

					}
					if (insertData) {
						
						try{

							stmt.executeUpdate();
							successCounter++;
						}
						catch (SQLException e){
							unsuccessResult.add("Baris ke " + r + " gagal di dimasukkan. Pastikan semua data valid pada baris tersebut.");
							continue;
						}
					}
				}
			}

			rs.close();
			stmt.close();
			currentCon.close();
		
		}
		catch(SQLException e){
			result.put("sql_exception", "sql_exception");
		}
		catch(IOException e){
			result.put("io_exception", "io_exception");
		}
		
		if (!unsuccessResult.isEmpty()){
			result.put("unsuccess_result", unsuccessResult);
		}
		
		result.put("success_result", successCounter + " data berhasil dimasukkan.");
		
		return result;
	}

	public boolean isValidDate(String inDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}

	public ArrayList<Integer> getAllProgramId() throws SQLException {
		ArrayList<Integer> allProgramId = new ArrayList<Integer>();
		ResultSet rs = null;
		Statement stmt = null;

		String query = "Select idprogram from program";
		stmt = currentCon.createStatement();

		rs = stmt.executeQuery(query);

		while (rs.next()) {
			allProgramId.add(rs.getInt(1));
		}

		if (rs != null)
			rs.close();

		stmt.close();
		return allProgramId;

	}

	public void changeDonatur(String before, String after) throws SQLException {
		ArrayList<Program> allProgram = new ArrayList<Program>();
		ResultSet rs = null;
		Statement stmt = null;
		PreparedStatement psmt = null;

		String query = "select * from program where donatur like '%" + before + "%'";
		stmt = currentCon.createStatement();
		rs = stmt.executeQuery(query);

		while (rs.next()) {
			int idProgram = rs.getInt(1);
			String[] donaturProgram = rs.getString(4).split("-");
			String newDonaturProgram = "";
			for (int i = 0; i < donaturProgram.length; i++) {
				if (donaturProgram[i].equals(before))
					newDonaturProgram += after + "-";
				else
					newDonaturProgram += donaturProgram[i] + "-";
			}
			newDonaturProgram = newDonaturProgram.substring(0, newDonaturProgram.length() - 1);

			String query2 = "update program set donatur = ? where idprogram='" + idProgram + "'";
			psmt = currentCon.prepareStatement(query2);

			psmt.setString(1, newDonaturProgram);
			int rowsUpdate = psmt.executeUpdate();
		}

		if (rs != null)
			rs.close();

		stmt.close();
	}
}
