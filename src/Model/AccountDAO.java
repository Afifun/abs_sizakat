package Model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ModABSQuery.Main;
import abs.backend.java.lib.runtime.ABSRuntime;
import abs.backend.java.lib.runtime.COG;

public class AccountDAO {
	static Connection currentCon = null;
	static ConnectionManager con = null;
	static ResultSet rs = null;
	String propertiesFileName = "config.properties";
	Properties properti = null;
	
	public AccountDAO() throws IOException {
		con = new ConnectionManager();
		currentCon = con.getConnection();
		properti = new Properties();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);
		properti.load(inputStream);		
	}

	public static Account login(Account adm) throws Exception {
		Statement stmt = null;
		CallABS call = new CallABS();
		String username = adm.getUsername();
		String password = adm.getPassword();		
		con = new ConnectionManager();
		currentCon = con.getConnection();
		String query;

		query = "select * from account where username='" + username
				+ "' AND password='" + password + "'";

		stmt = currentCon.createStatement();
		rs = stmt.executeQuery(query);
		boolean exist = rs.next();

		if (!exist) {
			adm.setValid(false);
		} else {
			adm.setValid(true);
			adm.setAdmin(rs.getString(4));
			adm.setId(rs.getString(1));
			adm.setNamaDonatur(rs.getString(5));
			System.out.println(rs.getString(4)+" MASUK GA SIH");
		}
		return adm;
	}
	
	public boolean delete(String id) throws SQLException {

		boolean isSucces = false;

		Statement stmt = null;
		String query = "DELETE FROM account where id = " + id;
		
		stmt = currentCon.createStatement();
		int rowsUpdate = stmt.executeUpdate(query);
		if (rowsUpdate > 0) {
			isSucces = true;
		}

		return isSucces;
	}

	
	public Account getInfoAccount(String id) throws Exception {

		Statement stmt = null;
		ResultSet rs = null;

		Account akun = new Account();

		String query = "Select * from account where id = " + id;
		stmt = currentCon.createStatement();

		rs = stmt.executeQuery(query);
		boolean exist = rs.next();

		if (exist) {
			akun = Account.load(rs);
		}
		stmt.close();

		return akun;
	}

	public boolean create(Account acc) throws Exception {
		con = new ConnectionManager();
		currentCon = con.getConnection();

		boolean isSucces = false;
		String username = acc.getUsername();
		String password = acc.getPassword();
		String namaDonatur = acc.getNamaDonatur();

		PreparedStatement psmt = null;

		String query = "insert into account values(?,?,?,?,?)";

		psmt = currentCon.prepareStatement(query);

		psmt.setString(1, null);
		psmt.setString(2, username);
		psmt.setString(3, password);
		psmt.setString(4, "donatur");
		psmt.setString(5, namaDonatur);

		int rowsUpdate = psmt.executeUpdate();

		if (rowsUpdate > 0) {
			isSucces = true;
		} else {
			System.out.println("GAK MASUK WOI");
		}
		return isSucces;

	}
	
	public List<Account> selectAllAccount() throws Exception {
		List<Account> list = new ArrayList<Account>();
		ResultSet rs = null;
		Statement stmt = null;

		String query = "Select * from account where role='donatur' ORDER BY id DESC";
		stmt = currentCon.createStatement();

		rs = stmt.executeQuery(query);
		while (rs.next()) {
			Account acc = new Account();
			// set fields of c
			acc = Account.load(rs);
			list.add(acc);
		}

		if (rs != null)
			rs.close();

		stmt.close();
		return list;

	}

	static String callABS(String attr, String table, String con)
			throws Exception {
		ABSRuntime runtime = new ABSRuntime();
		COG cog = runtime.createCOG(Main.class);
		Main m = new Main(cog);
		m.setSelect(attr, table, con);
		while (Main.select == false) {
			Thread.yield();
		}
		runtime.start(Main.class);
		while (Main.result == "") {
			Thread.yield();
		}
		return Main.result;
	}
	
	public boolean update(Account akun) throws SQLException, IOException {
		
		Statement stmt = null;

		boolean isSucces = false;
		int update;
		String id = akun.getId();		
		String namaDonatur = akun.getNamaDonatur();
		String password = akun.getPassword();
		String username = akun.getUsername();	
					
		
		String query = "update account set username = '" + username + "', password = '" + password + "', namaDonatur = '" + namaDonatur + "' where id ='" + id + "'";
		
		stmt = currentCon.createStatement();

		update = stmt.executeUpdate(query);	

		stmt.close();
		
		if(update > 0){
			isSucces = true;
		}		
		
		return isSucces;
	}
	
	public boolean isDuplicateUsername(String username) throws SQLException{
		Statement stmt = null;
		ResultSet rs;
		
		String query = "select * from account where username = '" + username +"'";
		stmt = currentCon.createStatement();
		rs = stmt.executeQuery(query);
		if(rs.next()) return true;
		else return false;
	}
	
	public ArrayList<Program> getProgramByDonatur(String donatur) throws SQLException{
		ArrayList<Program> listProgram = new ArrayList<Program>();
		
		Statement stmt = null;
		ResultSet rs;
		
		String query = "select * from program where donatur like '%" + donatur +"%'";
		stmt = currentCon.createStatement();
		rs = stmt.executeQuery(query);
		while(rs.next()){
			Program pgm = Program.load(rs);
			listProgram.add(pgm);
		}
		
		return listProgram;
	}
}
