package Model;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Program implements Comparable<Program> {
	final String OLD_FORMAT = "dd/MM/yyyy";
	final String NEW_FORMAT = "yyyy/MM/dd";
	private int id;
	private String nama;
	private int biaya;
	private String donatur;
	private String status;
	private Date tanggalMulai;
	private Date tanggalAkhir;
	private InputStream foto;
	private String fotoNama;
	private String asnaf;
	private String lokasi;
	private String tujuan;
	private String penanggungjawab;
	private int progress;

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTanggalMulaiDMY() {
		String tgl = "";
		SimpleDateFormat sdfr = new SimpleDateFormat("dd-MMM-yyyy");
		tgl = sdfr.format(tanggalMulai);
								
		return tgl;
	}
	
	public String getTanggalAkhirDMY() {
		String tgl = "";
		SimpleDateFormat sdfr = new SimpleDateFormat("dd-MMM-yyyy");
		tgl = sdfr.format(tanggalAkhir);
								
		return tgl;
	}

	public Date getTanggalMulai() {

		return tanggalMulai;
	}

	public void setTanggalMulai(Date tanggalMulai) {
		this.tanggalMulai = tanggalMulai;
	}

	public Date getTanggalAkhir() {
		return tanggalAkhir;
	}

	public void setTanggalAkhir(Date tanggalAkhir) {
		this.tanggalAkhir = tanggalAkhir;
	}

	public String getPenanggungjawab() {
		return penanggungjawab;
	}

	public void setPenanggungjawab(String penanggungjawab) {
		this.penanggungjawab = penanggungjawab;
	}

	public String getAsnaf() {
		return asnaf;
	}

	public void setAsnaf(String asnaf) {
		this.asnaf = asnaf;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getTujuan() {
		return tujuan;
	}

	public void setTujuan(String tujuan) {
		this.tujuan = tujuan;
	}

	public String getFotoNama() {
		return fotoNama;
	}

	public void setFotoNama(String fotoNama) {
		this.fotoNama = fotoNama;
	}

	public InputStream getFoto() {
		return foto;
	}

	public void setFoto(InputStream foto) {
		this.foto = foto;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getBiaya() {
		return biaya;
	}

	public void setBiaya(int biaya) {
		this.biaya = biaya;
	}

	public String getDonatur() {
		return donatur;
	}
	
	public ArrayList<String> getListDonatur(){
		ArrayList<String> listDonatur = new ArrayList<String>();
		String[] data = donatur.split("-");
		if(donatur.equals("")) return listDonatur;
		for(String temp : data){
			listDonatur.add(temp);
		}
		return listDonatur;
	}
	

	public void setDonatur(String donatur) {
		this.donatur = donatur;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static Program load(ResultSet rs) throws SQLException {

		Program pgm = new Program();

		String value = null;
		Date date = null;
		int budget;

		budget = rs.getInt(1);
		value = budget + "";
		if (value != null) {
			pgm.setId(budget);
		}

		value = rs.getString(2);
		if (value != null) {
			pgm.setNama(value);
		} else {
			pgm.setNama(null);
		}

		budget = rs.getInt(3);
		value = budget + "";
		if (value != null) {
			pgm.setBiaya(budget);
		}

		value = rs.getString(4);
		if (value != null) {
			pgm.setDonatur(value);
		} else {
			pgm.setDonatur(null);
		}

		value = rs.getString(5);
		if (value != null) {
			pgm.setStatus(value);
		} else {
			pgm.setStatus(null);
		}

		date = rs.getDate(6);
		if (date != null) {
			pgm.setTanggalMulai(date);
		}

		date = rs.getDate(7);
		if (date != null) {
			pgm.setTanggalAkhir(date);
		}
		value = rs.getString(8);
		if (value != null) {
			pgm.setFotoNama(value);
		}

		budget = rs.getInt(9);
		value = budget + "";
		if (value != null) {
			pgm.setProgress(budget);
		}

		value = rs.getString(10);
		if (value != null) {
			pgm.setAsnaf(value);
		}

		value = rs.getString(11);
		if (value != null) {
			pgm.setPenanggungjawab(value);
		}

		value = rs.getString(12);
		if (value != null) {
			pgm.setTujuan(value);
		}

		value = rs.getString(13);
		if (value != null) {
			pgm.setLokasi(value);
		}

		return pgm;
	}

	public String getTanggalBulan() {
		int bulan = this.tanggalMulai.getMonth();
		String tanggalBulan = this.tanggalMulai.getDate() + "";
		String[] daftarBulan = { "Jan", "Feb", "Mar", "Apr", "Mei", "Jun",
				"Jul", "Agu", "Sep", "Okt", "Nov", "Des" };
		return tanggalBulan + "<br>" + daftarBulan[bulan];
	}

	@Override
	public int compareTo(Program pgm) {
		// TODO Auto-generated method stub
		return this.nama.compareTo(pgm.nama);
	}
}
