package Model;

import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class Account {

	private String id;
	private String username;
	private String password;
	private boolean admin;
	private boolean valid;
	private String namaDonatur;
	private ArrayList<Program> listProgram;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		password = newPassword;
	}

	public String getUsername() {
		return username;
	}

	public void setUserName(String newUsername) {
		username = newUsername;
	}
	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean newValid) {
		valid = newValid;
	}
	
	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(String role) {
		if(role.equals("admin")) admin = true;
		else admin = false;
	}

	public String getNamaDonatur() {
		return namaDonatur;
	}

	public void setNamaDonatur(String namaDonatur) {
		this.namaDonatur = namaDonatur;
	}
	
	public static Account load(ResultSet rs) throws SQLException {
		Account acc = new Account();

		String value = null;
		
		value = rs.getString(1);
		if (value != null) {
			acc.setId(value);
		} else {
			acc.setId(null);
		}

		value = rs.getString(2);
		if (value != null) {
			acc.setUserName(value);
		} else {
			acc.setUserName(null);
		}

		value = rs.getString(3);
		if (value != null) {
			acc.setPassword(value);
		} else {
			acc.setPassword(null);
		}

		value = rs.getString(4);
		if (value != null) {
			acc.setAdmin(value);
		} else {
			acc.setAdmin(null);
		}

		value = rs.getString(5);
		if (value != null) {
			acc.setNamaDonatur(value);
		} else {
			acc.setNamaDonatur(null);
		}

		return acc;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public ArrayList<Program> getListProgram() throws IOException, SQLException{
		AccountDAO dao = new AccountDAO();
		ArrayList<Program> listProgram = dao.getProgramByDonatur(namaDonatur);
		Collections.sort(listProgram);
		return listProgram;
	}
}
