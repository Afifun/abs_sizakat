package ModABSQuery;
// ABSQuery.abs:3:1: 
public interface InterQuery_i extends abs.backend.java.lib.types.ABSInterface {
    // ABSQuery.abs:5:2: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getSelectQuery(abs.backend.java.lib.types.ABSString selectAttr, abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString constraint);
    // ABSQuery.abs:5:2: 
    public  abs.backend.java.lib.types.ABSString getSelectQuery(abs.backend.java.lib.types.ABSString selectAttr, abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString constraint);
    // ABSQuery.abs:6:2: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getDeleteQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString constraint);
    // ABSQuery.abs:6:2: 
    public  abs.backend.java.lib.types.ABSString getDeleteQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString constraint);
    // ABSQuery.abs:7:2: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getInsertQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString insertAttr, abs.backend.java.lib.types.ABSString value);
    // ABSQuery.abs:7:2: 
    public  abs.backend.java.lib.types.ABSString getInsertQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString insertAttr, abs.backend.java.lib.types.ABSString value);
    // ABSQuery.abs:8:2: 
    public  abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getUpdateQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString setAttribute, abs.backend.java.lib.types.ABSString constraint);
    // ABSQuery.abs:8:2: 
    public  abs.backend.java.lib.types.ABSString getUpdateQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString setAttribute, abs.backend.java.lib.types.ABSString constraint);
}
