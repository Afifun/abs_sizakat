package ModABSQuery;

import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSRef;
import abs.backend.java.observing.SystemObserver;
import abs.backend.java.scheduling.DefaultTaskScheduler;
import abs.backend.java.scheduling.GlobalScheduler;
import abs.backend.java.scheduling.GlobalSchedulingStrategy;
import abs.backend.java.scheduling.ScheduableTasksFilter;
import abs.backend.java.scheduling.ScheduableTasksFilterDefault;
import abs.backend.java.scheduling.ScheduleAction;
import abs.backend.java.scheduling.SimpleTaskScheduler;
import abs.backend.java.scheduling.TaskScheduler;
import abs.backend.java.scheduling.TaskSchedulerFactory;
import abs.backend.java.scheduling.TaskSchedulingStrategy;
import abs.backend.java.scheduling.TotalSchedulingStrategy;
import abs.backend.java.scheduling.UsesRandomSeed;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import abs.backend.java.lib.runtime.COG;
import abs.backend.java.lib.runtime.ABSRuntime;
import abs.backend.java.lib.runtime.ABSObject;
import abs.backend.java.lib.runtime.ABSMainCall;

public class Main extends abs.backend.java.lib.runtime.ABSObject {

	public static String result = "";
	public static String temperesult = "";

	public static boolean select = false;
	public static boolean update = false;
	public static boolean delete = false;
	public static boolean insert = false;

	/* variabel untuk selectQuery */
	public static String selectAttr = "";
	public static String selectTable = "";
	public static String selectConstraint = "";

	/* variabel untuk deleteQuery */
	public static String deleteTable = "";
	public static String deleteConstraint = "";

	/* variabel untuk insertQuery */
	public static String insertTable = "";
	public static String insertAttr = "";
	public static String insertValue = "";

	/* variabel untuk updateQuery */
	public static String updateTable = "";
	public static String updateSetAttr = "";
	public static String updateConstraint = "";

	public static void main(java.lang.String[] args) throws Exception {
		abs.backend.java.lib.runtime.StartUp.startup(args, Main.class);
	}

	public java.lang.String getClassName() {
		return "Main";
	}

	public java.util.List<java.lang.String> getFieldNames() {
		return java.util.Collections.EMPTY_LIST;
	}

	public Main(abs.backend.java.lib.runtime.COG cog) {
		super(cog);
	}

	public abs.backend.java.lib.types.ABSUnit run() {
		{

			// ABSString hasil = qr.getSelectQuery(ABSString.fromString("a"),
			// ABSString.fromString("b"), ABSString.fromString("c"));
			ModABSQuery.InterQuery_i qr = ModABSQuery.Query_c
					.__ABS_createNewObject(this);
			ABSString hasil = ABSString.fromString("");
			if (select == true) {
				hasil = qr.getSelectQuery(ABSString.fromString(selectAttr),
						ABSString.fromString(selectTable),
						ABSString.fromString(selectConstraint));
				// select = false;
				System.out.println(hasil.getString());
			}
			if (update == true) {
				hasil = qr.getUpdateQuery(ABSString.fromString(updateTable),
						ABSString.fromString(updateSetAttr),
						ABSString.fromString(updateConstraint));
				update = false;
			}
			if (insert == true) {
				hasil = qr.getInsertQuery(ABSString.fromString(insertTable),
						ABSString.fromString(insertAttr),
						ABSString.fromString(insertValue));
				// insert = false;
			}
			if (delete == true) {
				hasil = qr.getDeleteQuery(ABSString.fromString(deleteTable),
						ABSString.fromString(deleteConstraint));
				delete = false;
			}

			result = hasil.getString();
			temperesult = result;

			System.out.println(result);
			result = "";
			System.out.println("Novela Spalo Vianza");

		}
		return abs.backend.java.lib.types.ABSUnit.UNIT;
	}

	public void setSelect(String attr, String table, String constraint) {
		selectAttr = attr;
		selectTable = table;
		selectConstraint = constraint;
		System.out.println("Woi woi masuk sini kan, iya dong . . .");
		select = true;
	}

	public void setDelete(String table, String constraint) {
		deleteTable = table;
		deleteConstraint = constraint;

		delete = true;
	}

	public void setInsert(String attr, String table, String value) {
		insertTable = table;
		insertAttr = attr;
		insertValue = value;
		insert = true;
	}

	public void setUpdate(String attr, String table, String constraint) {
		updateTable = table;
		updateConstraint = constraint;
		updateSetAttr = attr;
		update = true;
	}

	public String getResult() {
		while (this.result == "") {
			Thread.yield();
		}
		return this.result;
	}
}
