package ModABSQuery;
import ModABSQuery.Main;

import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSRef;
import abs.backend.java.observing.SystemObserver;
import abs.backend.java.scheduling.DefaultTaskScheduler;
import abs.backend.java.scheduling.GlobalScheduler;
import abs.backend.java.scheduling.GlobalSchedulingStrategy;
import abs.backend.java.scheduling.ScheduableTasksFilter;
import abs.backend.java.scheduling.ScheduableTasksFilterDefault;
import abs.backend.java.scheduling.ScheduleAction;
import abs.backend.java.scheduling.SimpleTaskScheduler;
import abs.backend.java.scheduling.TaskScheduler;
import abs.backend.java.scheduling.TaskSchedulerFactory;
import abs.backend.java.scheduling.TaskSchedulingStrategy;
import abs.backend.java.scheduling.TotalSchedulingStrategy;
import abs.backend.java.scheduling.UsesRandomSeed;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import abs.backend.java.lib.runtime.COG;
import abs.backend.java.lib.runtime.ABSRuntime;
import abs.backend.java.lib.runtime.ABSObject;
import abs.backend.java.lib.runtime.ABSMainCall;

public class InputQuery
{
	public static void main(String[] args) throws Exception {
		ABSRuntime runtime = new ABSRuntime();
        COG cog = runtime.createCOG(Main.class);
		Main m = new Main(cog);
		m.setSelect("aaa","b","cc");
		while(m.select == false)
		{
			Thread.yield();
		}
		runtime.start(Main.class);
	}
}