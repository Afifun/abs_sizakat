package ModABSQuery;
// ABSQuery.abs:10:1: 
public final class Query_c extends abs.backend.java.lib.runtime.ABSObject implements abs.backend.java.lib.types.ABSClass, ModABSQuery.InterQuery_i {
    private static final java.lang.String[] __fieldNames = new java.lang.String[] { "resultQuery" };
    public final java.util.List<java.lang.String> getFieldNames() { return java.util.Arrays.asList(__fieldNames); }
    // ABSQuery.abs:12:2: 
    private abs.backend.java.lib.types.ABSString resultQuery;
    public Query_c() {
        getCOG().objectCreated(this);
    }
    protected final void __ABS_init() {
        this.resultQuery = abs.backend.java.lib.types.ABSString.fromString("");getCOG().objectInitialized(this);
    }
    protected final abs.backend.java.lib.types.ABSValue getFieldValue(java.lang.String __ABS_fieldName) throws java.lang.NoSuchFieldException {
        if ("resultQuery".equals(__ABS_fieldName)) return resultQuery;
        return super.getFieldValue(__ABS_fieldName);
    }
    public final java.lang.String getClassName() { return "Query"; }
    public static final <T extends Query_c> T createNewCOG() { return (T)Query_c.__ABS_createNewCOG(null, null); }
    public static final <T extends Query_c> T __ABS_createNewCOG(abs.backend.java.lib.runtime.ABSObject __ABS_source, abs.backend.java.scheduling.UserSchedulingStrategy strategy) {
        final abs.backend.java.lib.runtime.ABSRuntime __ABS_runtime = abs.backend.java.lib.runtime.ABSRuntime.getCurrentRuntime();
        final abs.backend.java.lib.runtime.COG __ABS_cog = strategy == null ? __ABS_runtime.createCOG(Query_c.class) : __ABS_runtime.createCOG(Query_c.class, strategy);
        final abs.backend.java.lib.runtime.ABSThread __ABS_thread = abs.backend.java.lib.runtime.ABSRuntime.getCurrentThread();
        final abs.backend.java.lib.runtime.COG __ABS_oldCOG = abs.backend.java.lib.runtime.ABSRuntime.getCurrentCOG();
        final abs.backend.java.lib.runtime.Task __ABS_sendingTask = abs.backend.java.lib.runtime.ABSRuntime.getCurrentTask();
        __ABS_thread.setCOG(__ABS_cog);
        try {
            Query_c __ABS_result = new Query_c();
            ;
            __ABS_runtime.cogCreated(__ABS_result);
            __ABS_cog.getScheduler().addTask(new abs.backend.java.lib.runtime.Task(new abs.backend.java.lib.runtime.ABSInitObjectCall(__ABS_sendingTask,__ABS_source,__ABS_result)));
            return (T)__ABS_result;
        } finally {
            __ABS_thread.setCOG(__ABS_oldCOG);
        }
    }
    public static final <T extends Query_c> T createNewObject() { return (T)Query_c.__ABS_createNewObject(null); }
    public static final <T extends Query_c> T __ABS_createNewObject(abs.backend.java.lib.runtime.ABSObject __ABS_source) {
        Query_c __ABS_result = new Query_c();
        __ABS_result.__ABS_init();
        return (T)__ABS_result;
    }
    // ABSQuery.abs:13:2: 
    public final abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getSelectQuery(abs.backend.java.lib.types.ABSString selectAttr, abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString constraint) {
        return (abs.backend.java.lib.runtime.ABSFut)abs.backend.java.lib.runtime.ABSRuntime.getCurrentRuntime().asyncCall(new abs.backend.java.lib.runtime.AbstractAsyncCallRT<ModABSQuery.Query_c>(
            this,
            abs.backend.java.lib.runtime.ABSRuntime.checkForNull(this),
            new ABS.StdLib.Duration_InfDuration(),
            new ABS.StdLib.Duration_InfDuration(),
            abs.backend.java.lib.types.ABSBool.FALSE) {
                abs.backend.java.lib.types.ABSString arg0;
                abs.backend.java.lib.types.ABSString arg1;
                abs.backend.java.lib.types.ABSString arg2;
                public java.util.List<abs.backend.java.lib.types.ABSValue> getArgs() {
                    return java.util.Arrays.asList(new abs.backend.java.lib.types.ABSValue[] {
                        arg0,arg1,arg2});
                }
                public abs.backend.java.lib.runtime.AsyncCall<?> init(abs.backend.java.lib.types.ABSString _arg0,abs.backend.java.lib.types.ABSString _arg1,abs.backend.java.lib.types.ABSString _arg2) {
                    arg0 = _arg0;
                    arg1 = _arg1;
                    arg2 = _arg2;
                    return this;
                }
                public java.lang.String methodName() {
                    return "getSelectQuery";
                }
                public Object execute() {
                    return target.getSelectQuery(arg0
                    ,arg1
                    ,arg2
                    );
                }
            }.init(selectAttr, table, constraint))
        ;
    }
    // ABSQuery.abs:13:2: 
    public final abs.backend.java.lib.types.ABSString getSelectQuery(abs.backend.java.lib.types.ABSString selectAttr, abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString constraint) {
        __ABS_checkSameCOG(); 
        if (__ABS_getRuntime().debuggingEnabled()) {
            abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
            __ABS_currentTask.newStackFrame(this, "getSelectQuery");
            __ABS_currentTask.setLocalVariable("selectAttr",selectAttr);
            __ABS_currentTask.setLocalVariable("table",table);
            __ABS_currentTask.setLocalVariable("constraint",constraint);
        }
         {
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",14);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",15);
            Query_c.this.resultQuery = abs.backend.java.lib.types.ABSString.fromString("SELECT ").add(selectAttr).add(abs.backend.java.lib.types.ABSString.fromString(" FROM ")).add(table).add(abs.backend.java.lib.types.ABSString.fromString(" ")).add(constraint);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",16);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
            return Query_c.this.resultQuery;
        }
    }
    // ABSQuery.abs:18:2: 
    public final abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getDeleteQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString constraint) {
        return (abs.backend.java.lib.runtime.ABSFut)abs.backend.java.lib.runtime.ABSRuntime.getCurrentRuntime().asyncCall(new abs.backend.java.lib.runtime.AbstractAsyncCallRT<ModABSQuery.Query_c>(
            this,
            abs.backend.java.lib.runtime.ABSRuntime.checkForNull(this),
            new ABS.StdLib.Duration_InfDuration(),
            new ABS.StdLib.Duration_InfDuration(),
            abs.backend.java.lib.types.ABSBool.FALSE) {
                abs.backend.java.lib.types.ABSString arg0;
                abs.backend.java.lib.types.ABSString arg1;
                public java.util.List<abs.backend.java.lib.types.ABSValue> getArgs() {
                    return java.util.Arrays.asList(new abs.backend.java.lib.types.ABSValue[] {
                        arg0,arg1});
                }
                public abs.backend.java.lib.runtime.AsyncCall<?> init(abs.backend.java.lib.types.ABSString _arg0,abs.backend.java.lib.types.ABSString _arg1) {
                    arg0 = _arg0;
                    arg1 = _arg1;
                    return this;
                }
                public java.lang.String methodName() {
                    return "getDeleteQuery";
                }
                public Object execute() {
                    return target.getDeleteQuery(arg0
                    ,arg1
                    );
                }
            }.init(table, constraint))
        ;
    }
    // ABSQuery.abs:18:2: 
    public final abs.backend.java.lib.types.ABSString getDeleteQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString constraint) {
        __ABS_checkSameCOG(); 
        if (__ABS_getRuntime().debuggingEnabled()) {
            abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
            __ABS_currentTask.newStackFrame(this, "getDeleteQuery");
            __ABS_currentTask.setLocalVariable("table",table);
            __ABS_currentTask.setLocalVariable("constraint",constraint);
        }
         {
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",19);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",20);
            Query_c.this.resultQuery = abs.backend.java.lib.types.ABSString.fromString("DELETE from ").add(table).add(abs.backend.java.lib.types.ABSString.fromString(" WHERE ")).add(constraint);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",21);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
            return Query_c.this.resultQuery;
        }
    }
    // ABSQuery.abs:23:2: 
    public final abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getInsertQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString insertAttr, abs.backend.java.lib.types.ABSString value) {
        return (abs.backend.java.lib.runtime.ABSFut)abs.backend.java.lib.runtime.ABSRuntime.getCurrentRuntime().asyncCall(new abs.backend.java.lib.runtime.AbstractAsyncCallRT<ModABSQuery.Query_c>(
            this,
            abs.backend.java.lib.runtime.ABSRuntime.checkForNull(this),
            new ABS.StdLib.Duration_InfDuration(),
            new ABS.StdLib.Duration_InfDuration(),
            abs.backend.java.lib.types.ABSBool.FALSE) {
                abs.backend.java.lib.types.ABSString arg0;
                abs.backend.java.lib.types.ABSString arg1;
                abs.backend.java.lib.types.ABSString arg2;
                public java.util.List<abs.backend.java.lib.types.ABSValue> getArgs() {
                    return java.util.Arrays.asList(new abs.backend.java.lib.types.ABSValue[] {
                        arg0,arg1,arg2});
                }
                public abs.backend.java.lib.runtime.AsyncCall<?> init(abs.backend.java.lib.types.ABSString _arg0,abs.backend.java.lib.types.ABSString _arg1,abs.backend.java.lib.types.ABSString _arg2) {
                    arg0 = _arg0;
                    arg1 = _arg1;
                    arg2 = _arg2;
                    return this;
                }
                public java.lang.String methodName() {
                    return "getInsertQuery";
                }
                public Object execute() {
                    return target.getInsertQuery(arg0
                    ,arg1
                    ,arg2
                    );
                }
            }.init(table, insertAttr, value))
        ;
    }
    // ABSQuery.abs:23:2: 
    public final abs.backend.java.lib.types.ABSString getInsertQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString insertAttr, abs.backend.java.lib.types.ABSString value) {
        __ABS_checkSameCOG(); 
        if (__ABS_getRuntime().debuggingEnabled()) {
            abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
            __ABS_currentTask.newStackFrame(this, "getInsertQuery");
            __ABS_currentTask.setLocalVariable("table",table);
            __ABS_currentTask.setLocalVariable("insertAttr",insertAttr);
            __ABS_currentTask.setLocalVariable("value",value);
        }
         {
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",24);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",25);
            Query_c.this.resultQuery = abs.backend.java.lib.types.ABSString.fromString("INSERT into ").add(table).add(abs.backend.java.lib.types.ABSString.fromString(" ( ")).add(insertAttr).add(abs.backend.java.lib.types.ABSString.fromString(" ) VALUES ( ")).add(value).add(abs.backend.java.lib.types.ABSString.fromString(" ) "));
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",26);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
            return Query_c.this.resultQuery;
        }
    }
    // ABSQuery.abs:28:2: 
    public final abs.backend.java.lib.runtime.ABSFut<abs.backend.java.lib.types.ABSString> async_getUpdateQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString setAttribute, abs.backend.java.lib.types.ABSString constraint) {
        return (abs.backend.java.lib.runtime.ABSFut)abs.backend.java.lib.runtime.ABSRuntime.getCurrentRuntime().asyncCall(new abs.backend.java.lib.runtime.AbstractAsyncCallRT<ModABSQuery.Query_c>(
            this,
            abs.backend.java.lib.runtime.ABSRuntime.checkForNull(this),
            new ABS.StdLib.Duration_InfDuration(),
            new ABS.StdLib.Duration_InfDuration(),
            abs.backend.java.lib.types.ABSBool.FALSE) {
                abs.backend.java.lib.types.ABSString arg0;
                abs.backend.java.lib.types.ABSString arg1;
                abs.backend.java.lib.types.ABSString arg2;
                public java.util.List<abs.backend.java.lib.types.ABSValue> getArgs() {
                    return java.util.Arrays.asList(new abs.backend.java.lib.types.ABSValue[] {
                        arg0,arg1,arg2});
                }
                public abs.backend.java.lib.runtime.AsyncCall<?> init(abs.backend.java.lib.types.ABSString _arg0,abs.backend.java.lib.types.ABSString _arg1,abs.backend.java.lib.types.ABSString _arg2) {
                    arg0 = _arg0;
                    arg1 = _arg1;
                    arg2 = _arg2;
                    return this;
                }
                public java.lang.String methodName() {
                    return "getUpdateQuery";
                }
                public Object execute() {
                    return target.getUpdateQuery(arg0
                    ,arg1
                    ,arg2
                    );
                }
            }.init(table, setAttribute, constraint))
        ;
    }
    // ABSQuery.abs:28:2: 
    public final abs.backend.java.lib.types.ABSString getUpdateQuery(abs.backend.java.lib.types.ABSString table, abs.backend.java.lib.types.ABSString setAttribute, abs.backend.java.lib.types.ABSString constraint) {
        __ABS_checkSameCOG(); 
        if (__ABS_getRuntime().debuggingEnabled()) {
            abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
            __ABS_currentTask.newStackFrame(this, "getUpdateQuery");
            __ABS_currentTask.setLocalVariable("table",table);
            __ABS_currentTask.setLocalVariable("setAttribute",setAttribute);
            __ABS_currentTask.setLocalVariable("constraint",constraint);
        }
         {
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",29);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",30);
            Query_c.this.resultQuery = abs.backend.java.lib.types.ABSString.fromString("UPDATE ").add(table).add(abs.backend.java.lib.types.ABSString.fromString(" SET ")).add(setAttribute).add(abs.backend.java.lib.types.ABSString.fromString(" WHERE ")).add(constraint);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().nextStep("D:\\nope\\Kerjaan\\abs\\Zakat\\ABSJsp\\ABSQuery.abs",31);
            if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
            return Query_c.this.resultQuery;
        }
    }
}
