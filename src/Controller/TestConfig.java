package Controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Connection;
import java.sql.SQLException;

import Model.ConnectionManager;

/**
 * Servlet implementation class TestConfig
 */
@WebServlet("/TestConfig")
public class TestConfig extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestConfig() {
        super();
        // TODO Auto-generated constructor stub
         
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String propertiesFileName = "config.properties";
		Properties properti = null;
		properti = new Properties();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);
		properti.load(inputStream);
		String user = properti.getProperty("dbuser");
		String pass = properti.getProperty("dbpassword");
		String dbname = properti.getProperty("dbname");
		String dbhost = properti.getProperty("dbhost");
		String imagepath = properti.getProperty("imgdir");
		String image2 = properti.getProperty("imgdir2");
		
		PrintWriter out = response.getWriter();
		out.println("dbuser : " + user);
		out.println("dbpass : " + pass);
		out.println("dbname : " + dbname);
		out.println("dbhost : " + dbhost);
		out.println("imagePath : " + imagepath);
		out.println("imagePath2 : " + image2);
		
		ConnectionManager cm = new ConnectionManager();
		Connection c = cm.getConnection();
		
		out.println(c.toString());
		
		try {
			out.println("Connection State : " + !c.isClosed());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

