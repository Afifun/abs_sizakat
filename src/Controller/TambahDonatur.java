package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Account;
import Model.AccountDAO;
import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class DetailEdit
 */
@WebServlet("/TambahDonatur")
public class TambahDonatur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TambahDonatur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}*/
		
		Program pgm = new Program();
		ProgramDAO dao = new ProgramDAO();
		
		String id = request.getParameter("idEdit");
		try {
			pgm = dao.getProgram(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		List<Account> listAcc = null;
		AccountDAO accDAO = new AccountDAO();
		try {
			listAcc = accDAO.selectAllAccount();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("program", pgm);
		request.setAttribute("listAcc", listAcc);
		request.getRequestDispatcher("tambahkanDonatur.jsp").forward(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		/*if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}*/
		
		Program pgm = new Program();
		ProgramDAO dao = new ProgramDAO();
		boolean notif = false;
		String nameNotif = "";
		
		String id = request.getParameter("idEdit");
		try {
			pgm = dao.getProgram(id);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String namaDonatur = !request.getParameter("namaDonatur").equals("") ? 
				request.getParameter("namaDonatur") : request.getParameter("namaDonaturLain");
		if(namaDonatur.equals("")) {}
		else{		
			String donaturSebelumnya = pgm.getDonatur();
			String daftarDonatur = namaDonatur + "-" + donaturSebelumnya;
			try {
				notif = dao.updateDonatur(id, daftarDonatur);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(notif)
				nameNotif = pgm.getNama();
		}
		
		request.setAttribute("nameNotif", nameNotif);
		request.setAttribute("notifDelete", notif);
		
		String site = "TambahDonatur?idEdit=" + request.getParameter("idEdit");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
		response.setHeader("Location", site);
	}
}
