package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Account;
import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class Delete
 */
@WebServlet("/DeleteDonaturDariProgram")
public class DeleteDonaturDariProgram extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteDonaturDariProgram() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}
		
		Program pgm = new Program();
		ProgramDAO dao = new ProgramDAO();
		boolean notif = false;
		String nameNotif = "";
		
		String id = request.getParameter("idProg");		
		System.out.println(id);
		String namaAkun = request.getParameter("namaAkun");
		System.out.println("yang di delete =" +  namaAkun);
		
		try {
			pgm = dao.getProgram(id);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		ArrayList<String> daftarDonatur = pgm.getListDonatur();
		String daftarDonaturBaru = "";				
		for(String item : daftarDonatur){
			if(!item.equals(namaAkun)){
				if(daftarDonaturBaru == "")
				{
					daftarDonaturBaru = item;
				}else{
					daftarDonaturBaru = daftarDonaturBaru + "-" + item;
				}								
			}
		}
			
			
		try {
			notif = dao.updateDonatur(id, daftarDonaturBaru);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(notif)
				nameNotif = pgm.getNama();
	
		
		request.setAttribute("nameNotif", nameNotif);
		request.setAttribute("notifDelete", notif);		
		
		String site = "TambahDonatur?idEdit=" + request.getParameter("idProg");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
		response.setHeader("Location", site);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
