package Controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Model.Account;
import Model.AccountDAO;
import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/UpdateAkun")
@MultipartConfig
public class UpdateAkun extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateAkun() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
		request.getRequestDispatcher("/EditAccount?idEdit=3").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}

		Account akun = new Account();
		AccountDAO dao = new AccountDAO();
		boolean notif = false;
		String nameNotif = "";

		String id = request.getParameter("id");
		String role = request.getParameter("role");
		String username = request.getParameter("username");
		String passwordLama = request.getParameter("passwordLama");
		String passwordBaru = request.getParameter("passwordBaru");
		String namaDonatur = request.getParameter("namaDonatur");
		String passwordSebelumnya = request.getParameter("passwordSebelumnya");

		akun.setId(id);
		akun.setNamaDonatur(namaDonatur);
		akun.setUserName(username);
		
		
		if (role.equals("admin")){
			akun.setAdmin(role);			
			if(passwordBaru.isEmpty()){
				akun.setPassword(passwordSebelumnya);
			}else{
				akun.setPassword(passwordBaru);
			}
			
			System.out.println(role);
			System.out.println(passwordBaru.isEmpty());
			System.out.println(passwordSebelumnya);
			System.out.println("masuk sini");

		} else {
			akun.setAdmin(role);
			if (passwordSebelumnya.equals(passwordLama)) {
				akun.setPassword(passwordBaru);
			}else{
				akun.setPassword(passwordSebelumnya);
			}
		}
		
		try {
			notif = dao.update(akun);
			if(notif){
				nameNotif = "Akun berhasil di perbarui";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			nameNotif = "Akun gagal di perbarui";
		}
		
		request.setAttribute("nameNotif", nameNotif);
		request.setAttribute("notifEdit", notif);
		request.getRequestDispatcher("editAkun.jsp").forward(request, response);	
				
	}
}
