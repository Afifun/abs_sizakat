package Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Account;
import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class ListProgram
 */
@WebServlet("/ListProgramDonatur")
public class ListProgramDonatur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListProgramDonatur() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		boolean notifDelete = false;
		boolean notifEdit = false;
		String nameNotif = "";
		int page = 1;
		int pageD = 1;
		int recordsPerPage = 8;
		String idDonatur ="";
		if(request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
		
		if(request.getParameter("pageD") != null)
            pageD = Integer.parseInt(request.getParameter("pageD"));
		
		
		if (request.getAttribute("notifDelete") != null) {
			notifDelete = (boolean) request.getAttribute("notifDelete");
			nameNotif = (String) request.getAttribute("nameNotif");
		}
		
		if (request.getAttribute("notifEdit") != null) {
			notifEdit = (boolean) request.getAttribute("notifEdit");
			nameNotif = (String) request.getAttribute("nameNotif");
		}
		
		List<Program> pgm = null;
		ProgramDAO dao = new ProgramDAO();
		try {
			pgm = dao.selectAllProgram();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Account acc = (Account) request.getSession().getAttribute("currentSessionUser");
		idDonatur = acc.getId();
		
		List<Program> list = null;
		ProgramDAO pdao = new ProgramDAO();
		try {
			list = pdao.selectSomeProgram((page-1)*recordsPerPage,recordsPerPage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		int noOfRecords = pgm.size()-1;
		
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        
        
        List<Program> listProgramDonatur = null;
		ProgramDAO pdaoDonatur = new ProgramDAO();
		try {
			listProgramDonatur = pdaoDonatur.selectProgramDonatur(acc.getNamaDonatur());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
        
		int noOfRecordsD = listProgramDonatur.size()-1;
		int noOfPagesD = (int) Math.ceil(noOfRecordsD * 1.0 / recordsPerPage);
		
		boolean checkEmpty = listProgramDonatur.isEmpty();
		
		request.setAttribute("notifEdit", notifEdit);
		request.setAttribute("notifDelete", notifDelete);
		request.setAttribute("nameNotif", nameNotif);
		request.setAttribute("program", pgm);
		request.setAttribute("programList", list);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);
        request.setAttribute("noOfPagesD", noOfPagesD);
        request.setAttribute("currentPageD", pageD);
        request.setAttribute("listProgramDonatur", listProgramDonatur);
        request.setAttribute("checkEmpty", checkEmpty);
        
        request.setAttribute("akun", acc); 
        request.setAttribute("idDon", idDonatur);
        
        
		
		if(acc != null){
			request.getRequestDispatcher("allProgramDonatur.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher("allProgram.jsp").forward(request, response);
		}
				
	}

}
