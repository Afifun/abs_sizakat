package Controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Account;
import Model.AccountDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("respon", "");
		request.getRequestDispatcher("login.jsp").forward(request,
				response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		Account acc = new Account();
		acc.setUserName(request.getParameter("username"));
		acc.setPassword(request.getParameter("password"));
		
		try {
			acc = AccountDAO.login(acc);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (acc.isValid()) {
			HttpSession session = request.getSession(true);
			session.setAttribute("currentSessionUser", acc);
			response.sendRedirect("Index");
			// logged-in page
		} else {
			if (acc.getPassword() != null) {
				request.setAttribute("respon", "Username/password anda salah");
				request.getRequestDispatcher("login.jsp").forward(request,
						response);
			} else {
				request.setAttribute("respon", "");
				request.getRequestDispatcher("login.jsp").forward(request,
						response);
			}

			// error page
		}
	}

}
