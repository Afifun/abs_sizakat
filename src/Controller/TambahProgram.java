package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Account;
import Model.AccountDAO;
import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class ListProgram
 */
@WebServlet("/TambahProgram")
public class TambahProgram extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TambahProgram() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}
		
		List<Program> pgm = null;
		ProgramDAO dao = new ProgramDAO();
		try {
			pgm = dao.selectAllProgram();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Account acc = null;
		AccountDAO acc_dao = new AccountDAO();
		String id = request.getParameter("idEdit");
		try {
			acc = acc_dao.getInfoAccount(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

		List<Program> listProgramDonatur = null;
		ProgramDAO pdaoDonatur = new ProgramDAO();
		try {
			listProgramDonatur = pdaoDonatur.selectProgramDonatur(acc.getNamaDonatur());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		ArrayList<String> daftarTersedia = new ArrayList<String>();
		
		for (Program item : listProgramDonatur) {
			daftarTersedia.add(item.getNama());			
		}
		for (String item : daftarTersedia) {
			System.out.println(item);			
		}
				
		request.setAttribute("program", pgm);
		request.setAttribute("akun", acc);
		request.setAttribute("daftar", daftarTersedia);
		request.setAttribute("listprogramdonatur", listProgramDonatur);		
		request.getRequestDispatcher("tambahkanProgramDonatur.jsp").forward(request,
				response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}
		
		Program pgm = new Program();
		ProgramDAO dao = new ProgramDAO();
		boolean notif = false;
		String nameNotif = "";
		
		String id = request.getParameter("idEdit");
		String donaturTambahan = request.getParameter("donaturTambahan");
		try {
			pgm = dao.getProgram(id);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String donaturSebelumnya = pgm.getDonatur();
		String daftarDonatur = donaturTambahan + "-" + donaturSebelumnya;
		try {
			notif = dao.updateDonatur(id, daftarDonatur);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(notif)
			nameNotif = pgm.getNama();	
		
		request.setAttribute("nameNotif", nameNotif);
		request.setAttribute("notifDelete", notif);
		
		String site = "TambahProgram?idEdit=" + request.getParameter("idDonatur");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
		response.setHeader("Location", site);
	}	

}
