package Controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Model.Account;
import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class Tambah
 */
@WebServlet("/Tambah")
@MultipartConfig
public class Tambah extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String propertiesFileName = "config.properties";
	Properties properti;

	/**
	 * @throws IOException 
	 * @see HttpServlet#HttpServlet()
	 */
	public Tambah() throws IOException {
		super();
		// TODO Auto-generated constructor stub
		 properti = new Properties();
		 InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);
		 properti.load(inputStream);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}
		
		request.getRequestDispatcher("inputProgram.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}
		
		Program pgm = new Program();
		ProgramDAO dao = new ProgramDAO();
		boolean notif = false;
		InputStream filecontent = null;
		String nameNotif = "";
		String []asnaf = request.getParameterValues("asnaf");
		String inputAsnaf = "";
		
		String mulai = request.getParameter("tanggalMulai");
		System.out.println(mulai);
		Date tanggalmulai = null;
		try {
			java.util.Date tglmulai = new SimpleDateFormat("yyyy-MM-dd")
					.parse(mulai);
			tanggalmulai = new java.sql.Date(tglmulai.getTime());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String akhir = request.getParameter("tanggalAkhir");
		Date tanggalakhir = null;
		try {
			java.util.Date tglakhir = new SimpleDateFormat("yyyy-MM-dd")
					.parse(akhir);
			tanggalakhir = new java.sql.Date(tglakhir.getTime());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Part filePart = request.getPart("foto");
		if (filePart.getSize() > 0) {
			filecontent = filePart.getInputStream();
			
		}else{
			filecontent = new FileInputStream(properti.getProperty("imgdir") + "non_found.jpg");
		}
		for (int i = 0; i < asnaf.length; i++) {
			if(i < asnaf.length-1)inputAsnaf += asnaf[i] + ",";
			else inputAsnaf += asnaf[i] ;
		}
		System.out.println(inputAsnaf);
		
		pgm.setNama(request.getParameter("nama"));
		pgm.setBiaya(Integer.parseInt(request.getParameter("biaya")));
		pgm.setDonatur(request.getParameter("donatur"));
		pgm.setStatus(request.getParameter("status"));
		pgm.setTanggalMulai(tanggalmulai);
		pgm.setTanggalAkhir(tanggalakhir);
		pgm.setFoto(filecontent);
		pgm.setProgress(Integer.parseInt(request.getParameter("progres")));
		pgm.setAsnaf(inputAsnaf);
		pgm.setPenanggungjawab(request.getParameter("penanggungjawab"));
		pgm.setTujuan(request.getParameter("tujuan"));
		pgm.setLokasi(request.getParameter("lokasi"));
		

		try {
			notif = dao.create(pgm);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(notif){
			nameNotif = request.getParameter("nama");
		}
		
		request.setAttribute("nameNotif", nameNotif);
		request.setAttribute("notif", notif);
		request.getRequestDispatcher("inputProgram.jsp").forward(request, response);

	}

}
