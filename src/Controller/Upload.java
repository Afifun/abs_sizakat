package Controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import Model.Account;
import Model.ProgramDAO;

/**
 * Servlet implementation class Upload
 */
@WebServlet("/Upload")
@MultipartConfig
public class Upload extends HttpServlet {
	String path = "";
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Upload() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}
		
		ProgramDAO dao = new ProgramDAO();
		//dao.upload("E:/coba.xls");
		
		Part part = request.getPart("excel");
		InputStream fileContent = part.getInputStream();
		String respon = "";
		String msg = "";
		HashMap<String, Object> hasil;

		try {
			hasil = dao.upload(fileContent);

			if(hasil.get("io_exception") != null){
				request.setAttribute("io_exception", hasil.get("io_exception"));
			}
			
			if(hasil.get("sql_exception") != null){
				request.setAttribute("sql_exception", hasil.get("sql_exception"));
			}
			
			request.setAttribute("success_result", hasil.get("success_result"));
			request.setAttribute("unsuccess_result", hasil.get("unsuccess_result"));
		} catch (Exception e) {
			request.setAttribute("invalid_format", true);
		}
		request.getRequestDispatcher("inputProgramExcel.jsp").forward(request, response);
		
	}

}
