package Controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Model.Account;
import Model.AccountDAO;
import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/EditAccountDonatur")
@MultipartConfig
public class EditAccountDonatur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditAccountDonatur() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute(
				"currentSessionUser");
		if (temp == null) {
			String site = "Index";
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}

		Account akun = new Account();
		AccountDAO dao = new AccountDAO();

		String idAkun = request.getParameter("idEdit");

		try {
			akun = dao.getInfoAccount(idAkun);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.setAttribute("akun", akun);

		request.getRequestDispatcher("editAkun.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute(
				"currentSessionUser");
		if (temp == null) {
			String site = "Index";
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}

		Account akun = temp != null && !temp.isAdmin() ? temp : new Account();
		AccountDAO dao = new AccountDAO();
		boolean notif = false;
		boolean akunSama = false;
		boolean passwordBeda = false;
		int nameNotif = 0;

		String id = request.getParameter("id");
		String role = request.getParameter("role");
		String username = request.getParameter("username");
		String passwordLama = request.getParameter("passwordLama");
		System.out.println(passwordLama.isEmpty());

		String passwordBaru = request.getParameter("passwordBaru");
		System.out.println(passwordBaru.isEmpty());
		String namaDonatur = request.getParameter("namaDonatur");
		String passwordSebelumnya = request.getParameter("passwordSebelumnya");
		System.out.println(passwordSebelumnya.isEmpty());
		System.out.println("----------------------------------------------");
		System.out.println("----------------------------------------------");
		System.out.println("----------------------------------------------");

		ProgramDAO pDAO = new ProgramDAO();

		try {
			/* kalo nama akun udah ada */
			List<Account> allAccount = dao.selectAllAccount();
			for (int i = 0; i < allAccount.size(); i++) {
				if (!akun.getNamaDonatur().equals(namaDonatur)
						&& allAccount.get(i).getNamaDonatur()
								.equals(namaDonatur)) {
					/* udah ada */					
					akunSama = true;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!passwordSebelumnya.equals(passwordLama)) {			
			passwordBeda = true;			
		}

		/* berarti dia gak niat ganti password */
		if (passwordLama.isEmpty() && passwordBaru.isEmpty()) {
			if (akunSama) {
				nameNotif = 1;
			} else {
				akun.setId(id);
				akun.setNamaDonatur(namaDonatur);
				akun.setUserName(username);
				akun.setPassword(passwordSebelumnya);

				try {
					notif = dao.update(akun);
					nameNotif = 5;
					if (notif) {
						request.getSession().setAttribute("currentSessionUser",
								akun);
					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		if (!passwordLama.isEmpty() && !passwordBaru.isEmpty()) {
			if (akunSama) {
				nameNotif = 1;
			} else {
				if (passwordBeda){
					nameNotif = 2;
				} else {
					akun.setId(id);
					akun.setNamaDonatur(namaDonatur);
					akun.setUserName(username);
					akun.setPassword(passwordBaru);

					try {
						notif = dao.update(akun);
						nameNotif = 5;
						if (notif) {
							request.getSession().setAttribute(
									"currentSessionUser", akun);
						}

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}

		request.setAttribute("akun", akun);
		request.setAttribute("akunSama", akunSama);
		request.setAttribute("passwordBeda", passwordBeda);
		request.setAttribute("nameNotif", nameNotif);
		request.setAttribute("notif", notif);
		request.setAttribute("namaDonatur", namaDonatur);
		request.getRequestDispatcher("editAkun.jsp").forward(request, response);
	}
}
