package Controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Account;
import Model.AccountDAO;

/**
 * Servlet implementation class TambahAkun
 */
@WebServlet("/TambahAkun")
@MultipartConfig
public class TambahAkun extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String propertiesFileName = "config.properties";
	Properties properti;

	/**
	 * @throws IOException 
	 * @see HttpServlet#HttpServlet()
	 */
	public TambahAkun() throws IOException {
		super();
		// TODO Auto-generated constructor stub
		 properti = new Properties();
		 InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);
		 properti.load(inputStream);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}
		
		request.getRequestDispatcher("createAccount.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}
		
		Account acc = new Account();
		AccountDAO dao = new AccountDAO();
		boolean notif = true;
		String nameNotif = "";
		boolean error = false;
		String typeNotif = "";
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String repeatPassword = request.getParameter("repeatPassword");
		String namaDonatur = request.getParameter("namaDonatur");

		if(password.equals(repeatPassword)){
			acc.setUserName(username);
			acc.setPassword(password);
			acc.setNamaDonatur(namaDonatur);
			try {
				if(!dao.isDuplicateUsername(username)){
					notif = dao.create(acc);
					nameNotif = "Akun "+username+" berhasil ditambahkan";
					error = false;
					typeNotif = "Success";
				}
				else
					typeNotif = "Username telah terdaftar";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{		
			nameNotif = "Kedua password tidak sama";
			error = true;
		}

		request.setAttribute("typeNotif", typeNotif);
		request.setAttribute("nameNotif", nameNotif);
		request.setAttribute("notif", notif);
		request.setAttribute("error", error);
		request.getRequestDispatcher("createAccount.jsp").forward(request, response);

	}

}
