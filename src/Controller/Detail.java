package Controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/Detail")
public class Detail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Detail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Program pgm = new Program();
		ProgramDAO dao = new ProgramDAO();
		String[] inputasnaf = new String[8];
		String asnaf = "";
		int biaya = 0;
		
		int previous = -1;
		int next = -1;
		
		String id = request.getParameter("idDetail");
		try {
			pgm = dao.getProgram(id);
			
			ArrayList<Integer> allProgramId = dao.getAllProgramId();
			int indexId = allProgramId.indexOf(Integer.parseInt(id));
			next = indexId+1<allProgramId.size() ? allProgramId.get(indexId+1) : -1;
			previous = indexId-1>=0 ? allProgramId.get(indexId-1) : -1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		biaya = pgm.getBiaya();

		asnaf = pgm.getAsnaf();
		inputasnaf = asnaf.split(",");

		DecimalFormat formatter = (DecimalFormat) DecimalFormat
				.getCurrencyInstance();

		DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

		formatRp.setCurrencySymbol("Rp. ");
		formatRp.setMonetaryDecimalSeparator(',');
		formatRp.setGroupingSeparator('.');

		formatter.setDecimalFormatSymbols(formatRp);

		String moneyString = formatter.format(biaya);
		
		
		request.setAttribute("asnaf", inputasnaf);
		request.setAttribute("biaya", moneyString);
		request.setAttribute("program", pgm);
		request.setAttribute("next", next);
		request.setAttribute("previous", previous);
		
		List<Program> list = null;
		ProgramDAO pdao = new ProgramDAO();
		try {
			list = pdao.gettopprogram();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		request.setAttribute("topprogram", list);
		request.getRequestDispatcher("detilProgram.jsp").forward(request,
				response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
