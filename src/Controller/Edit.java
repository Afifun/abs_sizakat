package Controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Model.Account;
import Model.Program;
import Model.ProgramDAO;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/Edit")
@MultipartConfig
public class Edit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Edit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Account temp = (Account) request.getSession().getAttribute("currentSessionUser");
		if(temp == null || !temp.isAdmin()){
			String site = "Index" ;
			response.setStatus(response.SC_MOVED_TEMPORARILY);
			response.setHeader("Location", site);
		}		
		
		Program pgm = new Program();
		ProgramDAO dao = new ProgramDAO();
		boolean notif = false;
		String nameNotif = "";
		String inputAsnaf = "";

		String mulai = request.getParameter("tanggalMulai");
		Date tanggalmulai = null;
		try {
			java.util.Date tglmulai = new SimpleDateFormat("yyyy-MM-dd").parse(mulai);
			tanggalmulai = new java.sql.Date(tglmulai.getTime());  
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String akhir = request.getParameter("tanggalAkhir");
		Date tanggalakhir = null;
		try {
			java.util.Date tglakhir = new SimpleDateFormat("yyyy-MM-dd").parse(akhir);
			tanggalakhir = new java.sql.Date(tglakhir.getTime());  
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Part filePart = request.getPart("foto");

		InputStream filecontent = filePart.getInputStream();
		
		for (int i = 1; i < 8; i++) {
			if (request.getParameter("asnaf" + i) != null) {
				if (i < 7)
					inputAsnaf += request.getParameter("asnaf" + i) + ",";
				else
					inputAsnaf += request.getParameter("asnaf" + i);
			}else{
				inputAsnaf += ",";
			}
		}

		System.out.println(inputAsnaf);
		
		pgm.setNama(request.getParameter("nama"));
		pgm.setBiaya(Integer.parseInt(request.getParameter("biaya")));
		pgm.setDonatur(request.getParameter("donatur"));
		pgm.setStatus(request.getParameter("status"));
		pgm.setTanggalMulai(tanggalmulai);
		pgm.setTanggalAkhir(tanggalakhir);
		pgm.setFoto(filecontent);
		pgm.setProgress(Integer.parseInt(request.getParameter("progres")));
		pgm.setAsnaf(inputAsnaf);
		pgm.setPenanggungjawab(request.getParameter("penanggungjawab"));
		pgm.setTujuan(request.getParameter("tujuan"));
		pgm.setLokasi(request.getParameter("lokasi"));
		pgm.setId(Integer.parseInt(request.getParameter("idEdit")));

		try {
			notif = dao.update(pgm);
			nameNotif = "berhasil";
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			notif = true;
			nameNotif = "error";
		}		
				
		System.out.println(nameNotif);
		request.setAttribute("notif", notif);
		request.setAttribute("nama", request.getParameter("nama"));
		request.setAttribute("nameNotif", nameNotif);
		
		
		request.getRequestDispatcher("edit_program.jsp").forward(request, response);
		
		/*String site = "DetailEdit?idEdit=" + request.getParameter("idEdit");		
		response.setStatus(response.SC_MOVED_TEMPORARILY);
		response.setHeader("Location", site);*/
	}

}
