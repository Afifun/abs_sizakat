# README #

## Developement Guide ##
### Pre-requisites ###
* Java Development Kit 1.7 atau yang lebih baru. 
* Git. Seperti github atau yang lainnya 
* Eclipse EE Juno atau yang lebih baru
* Mysql JDBC Driver 
* XAMPP
* [Apache-Tomcat-7.x.y](https://tomcat.apache.org/download-70.cgi) atau yang lebih baru


Untuk melakukan developement, ikuti langkah-langkah berikut:
## Import Project ##
* Clone project ABS_SiZakat di https://bitbucket.org/Afifun/abs_sizakat
* Setelah itu, buka eclipse dan import project ABS_SiZakat,
* Setelah proses import selesai, maka klik kanan pada project SistemInformasiZakat, kemudian pilih build-path dan klik configure build-path.
* Pada bagi tab Libraries, atur kembali path library yang bertanda merah, sehingga tanda merahnya hilang.
**note** : library tambahan yaitu absfrontend.jar dan mysql-connector-java-5.1.28.jar berada pada folder **extended-library**.

![2.png](https://bitbucket.org/repo/XMpzpg/images/3964699664-2.png)

## Membuat Local Database ##
* Buka XAMPP Control Panel, kemudian start Apache dan MySQL. Setelah itu, buka http://localhost/phpmyadmin melalui Browser,
* Buat database dengan nama ‘abs_sizakat’ (tanpa tanda petik),
* Kemudian, import file abs_sizakat.sql yang berada satu directori dengan project yang telah di-clone diatas.

## Membuat Server ##
* Kembali ke eclipse, kemudian klik Menu File, Pilih New dan Klik Others,
* Pilih Server
* Kemudian pada bagian Select the server type, double-click Apache dan pilih Tomcat v.7.0 Server atau yang terbaru.
* Isi Servers’s host name dengan localhost, Server name sesuai dengan keinginan.
* Kemuudian pada Server runtime environment, klik add. Maka akan muncul kotal dialog New Server Runtime Environment. Pada bagian Name, isi dengan nama server kita, kemudian pada Tomcat installation  directory, klik Browse kemudian pilih folder Apache-tomcat-x.y. Jika sudah selesai, klik Finish.
* Kemudian Add aplikasi kita ke server yang sudah dibuat dan klik Finish.

## Menjalankan Project ##
* Klik kanan aplikasi, dalam hal ini SistemInformasiZakat. Kemudian pilih Run As dan klik Run on Server.
* Pada kotak dialog Run On Server pilih server yang sudah kita buat sebelumnya dan kilik Next.
* Pastikan aplikasi (SistemInformasiZakat) berada pada kolom Configured. Jika tidak, maka Add aplikasi dan klik Finish.

## Files and Structures Guide ##
Pengembangan SistemInformasiZakat menggunakan pattern MVC (Model-View-Controller) yang dibagi ke dalam 3 direktori. View berada pada direktori WebContent, Controller pada direktori Controller dan Model pada direktori Model.

* ### View – /SistemInformasiZakat/WebContent ###

![1.png](https://bitbucket.org/repo/XMpzpg/images/3987789794-1.png)

Direktori berisi semua file .jsp yang berfungsi untuk mengatur UI pada aplikasi ini. Selain itu, pada direktori ini terdapat subdirektori assets dan images. Pada subdirektori assets berisi file css, javascript, font dan lain-lain yang digunakan oleh .jsp. Kemudian pada images menyimpan foto-foto program yang di-upload.

* ### Controller - /SistemInformasiZakat/src/Controller ###

![3.png](https://bitbucket.org/repo/XMpzpg/images/3908577503-3.png)

Pada direktori ini berisi class-class controller berupa servlet yang berfungsi untuk memanggil view (.jsp). Sebelum di-tampilkan ke view, semua data diolah pada direktori ini. Untuk mengetahui file .jsp mana yang dipanggil oleh servlet, dapat dilihat pada method getRequestDispacher yang dipanggil dimasing-masing servlet.

 

* ### Model - /SistemInformasiZakat/src/Model ###

![4.png](https://bitbucket.org/repo/XMpzpg/images/450996913-4.png)
 
Pada direktori ini berisi class-class Model pada SistemInformasiZakat. Untuk koneksi ke database diatur pada ConnectionManager.java. Kemuadian untuk operasi CRUD pada tiap-tipa model diatur pada *DAO.java