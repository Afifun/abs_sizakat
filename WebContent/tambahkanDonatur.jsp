<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">
<meta name="description" content="Simple responsive css framework">
<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

</head>

<jsp:include page="header.jsp" flush="false" />
<body>

	<!-- NAVBAR
================================================== -->
	<!-- Docs master nav -->
	<!-- Fixed navbar -->



	<div class="container" style="padding-top: 60px">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><a href="Index">Home</a></li>
				<li><a href="ListProgram">Daftar Donatur</a></li>
				<li class="active">Edit Akun</li>
			</ol>
		</div>
	</div>
	<div class="container">
		<div class="col-lg-12">
			<h2>${program.getNama()}</h2>
			<label><b>Donatur Program Ini</b></label> <br>
			<div class="well3" style="margin: 0">
			<div style="height: 220px; overflow: auto;">
				<c:choose>				
					<c:when test="${program.getListDonatur().size() == 0}">
						<i>Program ini belum memiliki donatur.</i>
					</c:when>
					<c:otherwise>
						<table>
							<c:forEach var="j" begin="0"
								end="${program.getListDonatur().size()-1}">
								<tr>
									<td>${program.getListDonatur().get(j)}</td>
									<td style="padding-left: 10px; padding-bottom: 10px;">
										<div class="modal fade"
											id="confirm-delete-${j}"
											tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
											aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">

													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal"
															aria-hidden="true">&times;</button>
														<h4 class="modal-title" id="myModalLabel">Konfirmasi
															Delete</h4>
													</div>

													<div class="modal-body">
														<p>
															Apakah Anda yakin ingin menghapus donatur <strong>${program.getListDonatur().get(j)}</strong>
															dari program <strong>${program.getNama()}</strong> ini?
														</p>
														<p class="debug-url"></p>
													</div>

													<div class="modal-footer">
														<button type="button" class="btn btn-cancel danger"
															data-dismiss="modal">Cancel</button>
														<a
															href="DeleteDonaturDariProgram?idProg=${program.getId()}&namaAkun=${program.getListDonatur().get(j)}"
															class="btn btn-danger danger">Delete</a>
													</div>
												</div>
											</div>
										</div>

										<button style="margin-right:10px" class="buttondelete" data-href="#" data-toggle="modal"
											data-target="#confirm-delete-${j}"
											href="#">
											<img src="assets/docs/icon/delete.png" style="width: 6px"></img>
										</button>
									</td>
								</tr>
							</c:forEach>
						</table>

					</c:otherwise>
				</c:choose>
			</div>
			</div>
			<br>

			<form style="width: 300px; margin-bottom: 50pt;" name="frm"
				method="post" action="TambahDonatur">
				<label><b>Tambah Donatur</b></label>
				<div class="form-group">
					<select name="namaDonatur" class="well2">
						<option value="">[Pilih Donatur Terdaftar]</option>
						<c:forEach var="i" begin="0" end="${listAcc.size()-1}">
							<c:choose>
								<c:when
									test="${program.getListDonatur().contains(listAcc.get(i).getNamaDonatur())}">

								</c:when>
								<c:otherwise>
									<option value="${listAcc.get(i).getNamaDonatur()}">${listAcc.get(i).getNamaDonatur()}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select> <a id="showmenu" class="btn btn-allprog"
						style="float: left; font-size: 13px;" type="submit"> Donatur
						Lainnya&nbsp;<img class="nextarrow" src="assets/docs/icon/add.png"></img>
					</a>
					<div class="clearfix"></div>
					<div class="menu" style="display: none;">
						<label><b>Donatur Lainnya</b></label>
						<div class="form-group">
							<input class="form-control" type="text"
								placeholder="nama donatur" name="namaDonaturLain"
								pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" autofocus>
						</div>
					</div>
				</div>


				<c:if test="${notif}">
					<div>
						<h2>${nameNotif}</h2>
					</div>
				</c:if>
				<input type="hidden" name="idEdit" value="${program.getId()}">
				<button class="btn btn-lg btn-primary submit2" type="submit">Simpan</button>
			</form>



		</div>
	</div>

	<script src="Metro-UI-CSS-master/js/hitua.js"></script>

</body>
<jsp:include page="footer.jsp" flush="false" />

<script>
	$(document).ready(function() {
		$('#showmenu').click(function() {
			$('.menu').slideToggle("slow");
		});
	});
</script>



<!-- Marketing messaging and featurettes
    ================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>
