<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">
<meta name="description" content="Simple responsive css framework">
<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

</head>

<jsp:include page="header.jsp" flush="false" />
<body>

	<!-- NAVBAR
================================================== -->
	<!-- Docs master nav -->
	<!-- Fixed navbar -->



	<div class="container" style="padding-top: 60px">
		<div class="col-lg-12">
			<ol class="breadcrumb" style="height: 35px;">
				<li><a href="Index">Home</a></li>
				<li><a href="ListAccount">Lihat Akun</a></li>
				<li class="active">Tambah Program</li>
			</ol>
		</div>
	</div>
	<div class="container" style="margin-bottom: 110px">
		<div class="col-lg-12">
			<!-- nama akun -->
			<label><b>Nama Akun</b></label> <br>
			<p class="well3">${akun.getNamaDonatur()}</p>

			<!-- program yang diikuti -->
			<label><b>Program yang dimiliki saat ini</b></label>
			
			
			<ul class="well3">
			<c:choose>
			<c:when test="${listprogramdonatur.size()==0}">
				<i>Anda tidak memiliki program apapun.</i>
			</c:when>
			<c:otherwise>
			
			<div style="height: 300px; overflow: auto;">
				<table>
					<c:forEach var="item" items="${listprogramdonatur}">
						<tr>
							<td>
								<li style="margin-left: 15px; font-size:15px">${item.getNama()}</li>
							</td>
							<td style="padding-left: 10px; padding-bottom: 10px;">
								<div class="modal fade"
									id="confirm-delete-${item.getId()}" tabindex="-1"
									role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">

											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true">&times;</button>
												<h4 class="modal-title" id="myModalLabel">Konfirmasi
													Delete</h4>
											</div>

											<div class="modal-body">
												<p>
													Apakah Anda yakin ingin menghapus program dengan <strong>${item.getNama()}</strong> dari <strong>${akun.getNamaDonatur()}</strong> ini?
												</p>
												<p class="debug-url"></p>
											</div>

											<div class="modal-footer">											
												<button type="button" class="btn btn-cancel danger"
													data-dismiss="modal">Cancel</button>
												<a href="DeleteProgramDariDonatur?idDelete=${item.getId()}&namaAkun=${akun.getNamaDonatur()}&idEdit=${akun.getId()}"
													class="btn btn-danger danger">Delete</a>
											</div>
										</div>
									</div>
								</div>

								<button style="margin-right:10px"class="buttondelete" data-href="#" data-toggle="modal"
															data-target="#confirm-delete-${item.getId()}"
															href="#">
									<img src="assets/docs/icon/delete.png" style="width: 6px"></img>
								</button>
							</td>
						</tr>

					</c:forEach>
				</table>
				</div>
			</c:otherwise>
			</c:choose>
			</ul>
			

			<!-- tambah program -->
			<div class="container">
				<form style="width: 300px; margin-bottom: 50pt;" name="frm"
					method="post" action="TambahProgram">
					<label><b>Tambah Program </b></label>
					<div class="form-group">
						<select name="idEdit" class="well2">
							<c:forEach var="i" begin="0" end="${program.size()-1}">
								<c:choose>
									<c:when test="${daftar.contains(program.get(i).getNama())}">
									</c:when>
									<c:otherwise>
										<option value="${program.get(i).getId()}">${program.get(i).getNama()}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<input type="hidden" name="donaturTambahan"
						value="${akun.getNamaDonatur()}"> <input type="hidden"
						name="idDonatur" value="${akun.getId()}">
					<button class="btn btn-lg btn-primary submit2" type="submit">Simpan</button>
					
				</form>


				<script src="Metro-UI-CSS-master/docs/js/hitua.js"></script>
				<script src="./Metro UI CSS   Metro Bootstrap CSS Library_files/hit"></script>

			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" flush="false" />
</body>



<script>
	
</script>


<!-- Marketing messaging and featurettes
    ================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>
