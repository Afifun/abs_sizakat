<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SiZakat</title>
</head>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="header.jsp" flush="false" />
<!-- CONTENT -->
<div class="container">
	<!-- JUDUL -->
	<div class="row" style="padding-top: 50pt;">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><a href="Index">Home</a></li>
				<li class="active">All Program</li>
			</ol>
		</div>

		<div class="col-lg-12">
			<h2>All Program</h2>
		</div>
	</div>


	<!-- ISI  -->
	<div class="row">
		<div class="row text-center">
			<c:forEach var="item" items="${programList}">
				<div class="col-lg-3 col-md-6">
					<div class="thumbnail">
					<c:choose>
	                        	<c:when test="${program.get(i).getFotoNama() != null}">
										<img class="imageProgramSize" src="${program.get(i).getFotoNama()}" alt="" >
								</c:when>
								<c:otherwise>
									<img class="imageProgramSize" src="images/not_available.jpg" alt="" >
								</c:otherwise>
							</c:choose>
						<div class="caption">
							<p class="home" style="border-radius: 50%;">${item.getTanggalBulan()}</p>
							<hr class="line">
							<h3>${item.getNama()}</h3>
							<p>${item.getTujuan()}</p>
							<p>
								<a href="Detail?idDetail=${item.getId()}"
									class="btn btn-default moreDetail">Selengkapnya</a>
							</p>
						</div>
					</div>
				</div>
			</c:forEach>

			<%--For displaying Previous link except for the 1st page --%>

		</div>
		<div class="row text-right">
			<div class="col-lg-12">
				<ul class="pagination">
					<%--For displaying Previous link except for the 1st page --%>
					<c:if test="${currentPage != 1}">
						<li><a href="program.do?page=${currentPage - 1}">Previous</a>
						</li>
					</c:if>

					<%--For displaying Page numbers. 
					    The when condition does not display a link for the current page--%>

					<c:forEach begin="1" end="${noOfPages}" var="i">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<li><a href="#"
									style="background-color: #008dd0; color: black">${i}</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="program.do?page=${i}">${i}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>


					<%--For displaying Next link --%>
					<c:if test="${currentPage lt noOfPages}">
						<li><a href="program.do?page=${currentPage + 1}">Next</a></li>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
	</div>
<jsp:include page="footer.jsp" flush="false" />
</body>

<!-- END OF ISI -->
</html>