<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>

</head>

<jsp:include page="header.jsp" flush="false" />
<body class="metro">

	<!-- NAVBAR
================================================== -->
	<!-- Docs master nav -->
	<!-- Fixed navbar -->
	<div class="container" style="padding-top: 60px">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><a href="Index">Home</a></li>
				<li class="active">Input Program via Form</li>
			</ol>
		</div>
	</div>
	<div class="container">
		<div class="col-lg-12">
			<h2>${program.getNama()}</h2>

			<jsp:include page="edit_form.jsp" flush="false" />
		</div>
	</div>	
	<script src="Metro-UI-CSS-master/js/hitua.js"></script>
</body>
<jsp:include page="footer.jsp" flush="false" />


<script>
	
</script>


<!-- Marketing messaging and featurettes
    ================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>
