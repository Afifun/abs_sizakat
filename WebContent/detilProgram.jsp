<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">
<meta name="description" content="Simple responsive css framework">
<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

<!-- Load JavaScript Libraries -->

<title>SiZakat</title>


</head>

<jsp:include page="header.jsp" flush="false" />

<div class="container">
	<div class="row" style="padding-top: 50pt;">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><a href="Index">Home</a></li>
				<li class="active">Detail Program</li>
			</ol>
		</div>

		<div class="col-lg-12">
			<h2>${program.getNama()}</h2>
		</div>
	</div>

	<div class="row">

		<div class="col-lg-8">

			<!-- the actual blog post: title/author/date/content -->
			<p>
				<i class="fa fa-clock-o"></i> Program dimulai pada ${ program.getTanggalMulaiDMY() } dan berakhir pada ${ program.getTanggalAkhirDMY() } 
				
			</p>
			<hr>
			
				<c:choose>
						<c:when test="${program.getFotoNama() != null}">
								<img src="${program.getFotoNama()}" alt=""  class="img-responsive">
						</c:when>
						<c:otherwise>
									<img src="images/not_available.jpg" alt="" class="img-responsive">
						</c:otherwise>
				</c:choose>
			<hr>

			<div class="well">	
				<div>
					<table class="table striped dataTable" id="dataTables-1">

						<tbody>
							<tr>

								<td><b>Tujuan</b></td>
								<td><b>:</b></td>
								<td>${program.tujuan}</td>
							</tr>
							<tr>
								<td><b>Biaya</b></td>
								<td><b>:</b></td>
								<td>${biaya}</td>
							</tr>
							<tr>

								<td><b>Ashnaf</b></td>
								<td><b>:</b></td>
								<td>
									<table>
										<tr></tr>
										<tr>
											<td class="tdAshnaf">
												<div>
													<p class="notBold"> <input type="checkbox" value="fkrmiskin"
														<c:if test="${asnaf[0] == 'fakirmiskin'}">
																	checked = "checked"
															</c:if>
														disabled />Fakir Miskin
													</p> 
													<p class="notBold"> <input type="checkbox" value="amil"
														<c:if test="${asnaf[1] == 'amil'}">
																	checked = "checked"
															</c:if>
														disabled />Amil
													</p> 
													<p class="notBold"> <input type="checkbox" value="muallaf"
														<c:if test="${asnaf[2] == 'muallaf'}">
																	checked = "checked"
															</c:if>
														disabled />Muallaf
													</p> 
													<p class="notBold"> <input type="checkbox" value="riqab"
														<c:if test="${asnaf[3] == 'riqab'}">
																	checked = "checked"
															</c:if>
														disabled />Riqab
													</p>
												</div>
											</td>

											<td class="tdAshnaf">
												<div>
													<p class="notBold"> <input type="checkbox" value="gharim"
														<c:if test="${asnaf[4] == 'gharim'}">
																checked = "checked"
														</c:if>
														disabled />Gharim
													</p> 
													<p class="notBold"> <input type="checkbox"
														value="fisabilillah"
														<c:if test="${asnaf[5] == 'fisabilillah'}">
																	checked = "checked"
															</c:if>
														disabled />Fisabilillah
													</p> 
													<p class="notBold"> <input type="checkbox" value="ibnusabil"
														<c:if test="${asnaf[6] == 'ibnusabil'}">
																	checked = "checked"
															</c:if>
														disabled />Ibnu Sabil
													</p>

												</div>
											</td>
										</tr>

									</table>
							<tr>

								<td><b>Status</b></td>
								<td><b>:</b></td>
								<td><c:if test="${program.status == 'Belum Berjalan'}">
						Belum Berjalan
							</c:if> <c:if test="${program.status == 'Sedang Berjalan'}">
						Sedang Berjalan
							</c:if> <c:if test="${program.status  == 'Selesai'}">
						Selesai
							</c:if></td>
							</tr>
							<tr>

								<td><b>Lokasi</b></td>
								<td><b>:</b></td>
								<td>${program.lokasi}</td>
							</tr>

							<c:if test="${currentSessionUser != null}">
							<tr>

								<td><b>Penanggung Jawab</b></td>
								<td><b>:</b></td>
								<td>${program.penanggungjawab}</td>
							</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>




			<!-- pager -->
			<ul class="pager">
			<!-- handlenya masih salah -->
				<c:if test="${previous != -1}">
				<li class="previous"><a href="Detail?idDetail=${previous}">&larr; Older</a></li>
				</c:if>
				<c:if test="${next != -1}">
					<li class="next"><a href="Detail?idDetail=${next}">Newer &rarr;</a></li>
				</c:if>				
			</ul>

			<hr>

		</div>

		<div class="col-lg-4">
			<div class="well">
				<h2>Program Terbaru</h2>
				<div class="block3">
					<img src="${topprogram.get(0).getFotoNama()}" alt=""
						class="img_inner noresize fleft">
					<div class="extra_wrapper">
						<div class="text1 color1">
							<time style="font-size:10pt" datetime="2014-01-01">Dimulai pada tanggal ${topprogram.get(0).getTanggalMulaiDMY()}</time>
							<a style="font-size:12pt"href="Detail?idDetail=${topprogram.get(0).getId()}">${topprogram.get(0).getNama()}</a>
						</div>						
					</div>
				</div>
				<div class="block3">
					<img src="${topprogram.get(1).getFotoNama()}" alt=""
						class="img_inner noresize fleft">
					<div class="extra_wrapper">
						<div class="text1 color1">
							<time style="font-size:10pt" datetime="2014-01-01">Dimulai pada tanggal ${topprogram.get(1).getTanggalMulaiDMY()}</time>
							<a style="font-size:12pt"href="Detail?idDetail=${topprogram.get(1).getId()}">${topprogram.get(1).getNama()}</a>
						</div>						
					</div>
				</div>
				<div class="block3">
					<img src="${topprogram.get(2).getFotoNama()}" alt=""
						class="img_inner noresize fleft">
					<div class="extra_wrapper">
						<div class="text1 color1">
							<time style="font-size:10pt" datetime="2014-01-01">Dimulai pada tanggal ${topprogram.get(2).getTanggalMulaiDMY()}</time>
							<a style="font-size:12pt"href="Detail?idDetail=${topprogram.get(2).getId()}">${topprogram.get(2).getNama()}</a>
						</div>						
					</div>
				</div>
			</div>
		</div>
		
</div>
</div>	
		
<jsp:include page="footer.jsp" flush="false" />			
</html>
