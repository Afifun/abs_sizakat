<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!-- FOOTER -->
<div class="container-black">
    <div class="container">
        <div class="row" style="padding-top:10px">
            <div class="col-md-4">            	
                <h4 style="margin-bottom:0px; color:#008dd0">Sistem Informasi Zakat</h4>
                SIZakat atau Sistem Informasi Zakat merupakan website penyedia laporan program zakat Lembaga Kemanusiaan Nasional PKPU.
                <br>
                <br>
                <br>
                <a style="color:#008dd0">Syarat dan ketentuan </a> <strong class="putih">|</strong> 
                <a style="color:#008dd0">Kebijakan Privasi </a><strong class="putih">|</strong> 
                <a style="color:#008dd0">FAQ</a> 
                
                
            </div>
            
            <div class="col-md-4" style="text-align: center;margin-top: 80px;">
            <p>Powered by<br />
            &copy; 2015 FMSE, Lab.<br />
            Fasilkom UI</p>
            </div>
            <div class="col-md-4" style="text-align: right;">
            <h5 style="margin-bottom:0px; color:#008dd0">Kami dapat dihubungi melalui:</h5>
            <address>
            		Kantor Pusat:
                    <br>Jl. Condet Raya No.27
                    <br>Kramatjati
                    <br>
                </address>                
                    <abbr title="Phone">Phone : </abbr> 0804 100 2000
                    <br>
                    <abbr title="Email">E-mail :</abbr> <a href="mailto:welcome@pkpu.or.id">welcome@pkpu.or.id</a>
                </address>
            </div>
            
        </div>
    </div>    
</div>
<!-- END OF FOOTER -->