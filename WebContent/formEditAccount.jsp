<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<!-- saved from url=(0033)http://metroui.org.ua/notify.html -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">
<meta name="description" content="Simple responsive css framework">
<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">



<!-- Load JavaScript Libraries -->
<script src="Metro-UI-CSS-master/docs/js/jquery/jquery.min.js"></script>
<script src="Metro-UI-CSS-master/docs/js/jquery/jquery.widget.min.js"></script>
<script src="Metro-UI-CSS-master/docs/js/jquery/jquery.mousewheel.js"></script>
<script src="Metro-UI-CSS-master/docs/js/prettify/prettify.js"></script>

<!-- Metro UI CSS JavaScript plugins -->
<script src="Metro-UI-CSS-master/docs/js/load-metro.js"></script>

<!-- Local JavaScript -->
<script src="Metro-UI-CSS-master/docs/js/docs.js"></script>
<script src="Metro-UI-CSS-master/docs/js/github.info.js"></script>
<script src="assets/js/jquery-1.10.2.min.js"></script>

<title>SiZakat</title>
<script src="Metro-UI-CSS-master/docs/js/metro.min.js"></script>
</head>
<div class="container">

	
	<c:choose>
	<c:when test="${akunSama}">
		<div class="alert alert-warning">		
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			Nama donatur <strong>${namaDonatur}</strong> sudah ada. 
		</div>
	</c:when>	
	<c:when test="${nameNotif == 5}">
		<div class="alert alert-success">		
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			<strong>Success!</strong> Akun  <strong>${akun.getNamaDonatur()}</strong> berhasil diperbarui.  
		</div>
	</c:when>
	
	</c:choose>
			
	<form style="width: 300px; margin-bottom: 50pt;" name="frm"
		method="post" action="EditAccount" enctype="multipart/form-data">
		<label><b>Nama Donatur</b></label>


		<div class="form-group">
			<input class="form-control" type="text" placeholder="Nama Donatur"
				value="${akun.getNamaDonatur()}" name="namaDonatur"
				pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" required autofocus>
		</div>

		<c:if test="${currentSessionUser.isAdmin()}">
			<label><b>Username</b></label>
			<div class="form-group">
				<input class="form-control" type="text"
					value="${akun.getUsername()}" name="username"
					pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" required autofocus>
			</div>

			<label><b>Password Baru </b><i> (optional)</i></label>
			<div class="form-group">
				<input class="form-control" type="password"
					placeholder="password baru" name="passwordBaru"
					pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" autofocus>
			</div>
		</c:if>

		<c:if test="${!currentSessionUser.isAdmin()}">
			<a id="showmenu" class="btn btn-allprog" style="font-size: 13px;"
				type="submit"> Ubah Password <sup style="font-size:9px">optional</sup>&nbsp;<img class="nextarrow"
				src="assets/docs/icon/key.png"></img>
			</a>
			<div class="clearfix"></div>
			<div class="menu" style="display: none;">
				<label><b>Password Lama</b></label>
				<div class="form-group">
					<input class="form-control" type="password"
						placeholder="Password Lama" name="passwordLama"
						pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" autofocus>
				</div>

				<label><b>Password Baru </b></label>
				<div class="form-group">
					<input class="form-control" type="password"
						placeholder="Password Baru" name="passwordBaru"
						pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" autofocus>
				</div>
			</div>
		</c:if>

		<input type="hidden" value="${akun.getId()}" name="id" /> <input
			type="hidden" value="${akun.getPassword()}" name="passwordSebelumnya" />
		<c:if test="${!currentSessionUser.isAdmin()}">
			<input type="hidden" value="${akun.getUsername()}" name="username" />
			<input type="hidden" value="donatur" name="role" />
		</c:if>
		<c:if test="${currentSessionUser.isAdmin()}">
			<input type="hidden" value="${akun.getPassword()}"
				name="passwordSebelumnya" />
			<input type="hidden" value="admin" name="role" />
		</c:if>

		<button class="btn btn-lg btn-primary submit2" type="submit">Simpan</button>

	</form>

	<script>
	    $(document).ready(function() {
        $('#showmenu').click(function() {
                $('.menu').slideToggle("slow");
        });
    });
	    </script>


	<script src="Metro-UI-CSS-master/docs/js/hitua.js"></script>
	<script src="./Metro UI CSS   Metro Bootstrap CSS Library_files/hit"></script>
</div>
</html>