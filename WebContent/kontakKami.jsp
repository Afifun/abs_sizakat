<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">
<meta name="description" content="Simple responsive css framework">
<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

<link href="Metro-UI-CSS-master/docs/css/metro-bootstrap.css"
	rel="stylesheet">

<link href="Metro-UI-CSS-master/docs/css/iconFont.css" rel="stylesheet">
<link href="Metro-UI-CSS-master/css/ganteng.css" rel="stylesheet">
<!--<link href="Metro-UI-CSS-master/docs/css/docs.css" rel="stylesheet">-->
<!--  <link href="Metro-UI-CSS-master/docs/css/jquery.dataTables.css"
	rel="stylesheet">-->

<title>SiZakat</title>
</head>



<body class="metro" style="padding-top:60px;">

<div class="container">
<div class="col-lg-12">                
                <ol class="breadcrumb">
                    <li><a href="Index">Home</a>
                    </li>
                    <li class="active">Kontak Kami</li>
                </ol>
            </div>
            </div>
<jsp:include page="header.jsp" flush="false"/>
	<div class="container">

		<div class="main-content clearfix">
		<h2 class="tile-group-title fg-orange">Temukan kami di sini:</h2>
			<div class="tile-area no-padding clearfix">
				<div class="tile-group no-margin no-padding clearfix"
					style="width: 100%">
					
					<div class="span4">
                        
                        <div class="panel">
                            <div class="panel-header bg-lightBlue fg-white">
                                <div class="judul">Head Office</div>
                            </div>
                            <iframe style="padding-right:35px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.7853588056205!2d106.85633180000002!3d-6.291916599999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f27bcb5ccfef%3A0x5ced97088f704d53!2sJalan+Condet+Raya+No.27%2C+Kramatjati%2C+Kota+Jakarta+Timur%2C+DKI+Jakarta+13520!5e0!3m2!1sid!2sid!4v1411547936852" width="1200" height="450" frameborder="0" style="border:0"></iframe>
                            <div class="kontak">
                            	<ul>
                            		<li><b>0804 100 2000</b></li>
                            		<li><b>(021) 87780013</b></li>
                            		<li><b>welcome@pkpu.or.id</b></li>
                            		
                            	</ul>
                            </div>   
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	
	<script src="Metro-UI-CSS-master/docs/js/hitua.js"></script>

</body>
<jsp:include page="footer.jsp" flush="false" />
</html>
