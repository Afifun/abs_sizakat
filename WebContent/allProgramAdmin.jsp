<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<title>SiZakat</title>
<link href="assets/docs/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="assets/docs/css/inline-hidden.css" rel="stylesheet">
</head>

<jsp:include page="header.jsp" flush="false" />

<body id="wrapper">
	<div class="container">
		<div id="page-wrapper">
			<div class="row" style="padding-top: 50pt;">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="Index">Home</a></li>
						<li class="active">Semua Program</li>
					</ol>
				</div>
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="col-lg-10">
							<h2>Semua Program</h2>
						</div>
						<div class="col-lg-2">
							<form action="Download" style="margin-bottom: 0px">
								<input class="button medium primary" type="hidden">
								<button class="btn btn-allprog" style="font-size: 13px;"
									type="submit">
									Download &nbsp;<img class="nextarrow"
										src="assets/docs/icon/download.png"></img>
								</button>
							</form>
						</div>
						<div class="col-lg-12">
							<div>
								<div class="panel-body well">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover"
											id="dataTables-example">
											<thead>
												<tr class="judulTable">
													
													<th style="text-align: center; vertical-align: middle;">ID</th>
													<th style="text-align: center; vertical-align: middle;">Nama
														Program</th>
													<th style="text-align: center; vertical-align: middle;">Status</th>
													<th style="text-align: center; vertical-align: middle;">List
														Donatur</th>
													<th style="text-align: center; vertical-align: middle;">Edit</th>
													<th style="text-align: center; vertical-align: middle;">Delete</th>

												</tr>
											</thead>

											<tbody>
												<!-- program -->
												<c:if test="${program.size() > 0}">
												<c:forEach var="i" begin="0" end="${program.size()-1}">
													<tr>
														<td>
															<div class="block3">
																<div class="extra_wrapper">${program.get(i).getId()}</div>
															</div>
														</td>
														<td>
															<div class="block3">
															<c:choose>
																	<c:when
																		test="${program.get(i).getFotoNama() != null}">
																		<img src="${program.get(i).getFotoNama()}" alt=""
																	class="img_inner noresize fleft">
																	</c:when>
																	<c:otherwise>
																		<img src="images/not_available.jpg" alt=""
																	class="img_inner noresize fleft">
																	</c:otherwise>
																</c:choose>
																
																<div class="extra_wrapper">
																	<div class="text1 color1">
																		<p style="font-size: 10pt">Program ini dimulai
																			sejak ${program.get(i).getTanggalMulaiDMY()} dan
																			berakhir pada ${program.get(i).getTanggalAkhirDMY()}
																		</p>
																		<a href="Detail?idDetail=${program.get(i).getId()}">${program.get(i).getNama()}</a>
																	</div>
																	${program.get(i).getTujuan()}
																</div>
															</div>

														</td>

														<!-- status -->
														<td>${program.get(i).getStatus()}</td>

														<!-- list donatur -->
														<td>



															<div style="max-height: 200px; width: 300px; overflow: auto;">
																<c:choose>
																	<c:when
																		test="${program.get(i).getListDonatur().size() != 0}">
																		<c:forEach var="j" begin="0"
																			end="${program.get(i).getListDonatur().size()-1}">
																			<ul style="margin-left: -20px;">
																				<li>${program.get(i).getListDonatur().get(j)}</li>
																			</ul>
																		</c:forEach>
																	</c:when>
																	<c:otherwise>
																		<i>Program ini belum memiliki donatur.</i>
																	</c:otherwise>
																</c:choose>
															</div>
															<form action="TambahDonatur" style="margin-bottom: 0px">
																<input class="button medium primary" type="hidden"
																	name="idEdit" value="${program.get(i).getId()}">
																<button class="btn btn-allprog"
																	style="font-size: 13px; padding: 2px" type="submit">
																	<img class="nextarrow" src="assets/docs/icon/add.png"
																		style="width: 10px"></img>
																</button>
															</form>
														</td>

														<!-- edit detail -->
														<td><form action="DetailEdit"
																style="margin-bottom: 0px">
																<input class="button medium primary" type="hidden"
																	name="idEdit" value="#"> <a
																	href="DetailEdit?idEdit=${program.get(i).getId()}"
																	class="btn btn-allprog" style="font-size: 13px;"
																	type="submit"> Edit &nbsp;<img class="nextarrow"
																	src="assets/docs/icon/edit.png"></img>
																</a>
															</form></td>


														<!-- delete -->

														<!-- delete akun -->
														<td>
															<div class="modal fade"
																id="confirm-delete-${program.get(i).getId()}"
																tabindex="-1" role="dialog"
																aria-labelledby="myModalLabel" aria-hidden="true">
																<div class="modal-dialog">
																	<div class="modal-content">

																		<div class="modal-header">
																			<button type="button" class="close"
																				data-dismiss="modal" aria-hidden="true">&times;</button>
																			<h4 class="modal-title" id="myModalLabel">Konfirmasi
																				Delete</h4>
																		</div>

																		<div class="modal-body">
																			<p>
																				Apakah Anda yakin ingin menghapus program dengan <strong>${program.get(i).getNama()}</strong>?
																			</p>
																			<p class="debug-url"></p>
																		</div>

																		<div class="modal-footer">
																			<button type="button" class="btn btn-cancel danger"
																				data-dismiss="modal">Cancel</button>
																			<a href="Delete?idDelete=${program.get(i).getId()}"
																				class="btn btn-danger danger">Delete</a>
																		</div>
																	</div>
																</div>
															</div> <a class="btn btn-allprog" style="font-size: 13px;"
															data-href="#" data-toggle="modal"
															data-target="#confirm-delete-${program.get(i).getId()}"
															href="#">Delete &nbsp;<img class="nextarrow"
																src="assets/docs/icon/delete.png"></img></a><br>
														</td>
													</tr>
												</c:forEach>
												</c:if>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>

		<!-- Page-Level Plugin Scripts - Tables -->
		<script src="assets/docs/js/jquery.dataTables.js"></script>
		<script src="assets/docs/js/dataTables.bootstrap.js"></script>

		<script>
			$(document).ready(
					function() {

						$('#dataTables-example').dataTable(
								{
									

									"iDisplayLength" : 5,
									"aLengthMenu" : [ [ 5, 10, 25, 50, -1 ],
											[ 5, 10, 25, 50, "All" ] ]
								}).fnSort([ [ 0, 'asc' ] ]);

					});
		</script>

	</div>


	<jsp:include page="footer.jsp" flush="false" />



</body>
</html>

