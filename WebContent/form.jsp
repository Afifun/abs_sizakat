<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<!-- saved from url=(0033)http://metroui.org.ua/notify.html -->
<html>
<div class="row" style="margin-bottom:50px">
	<div class="col-lg-12" style="padding-right: 50px; padding-left: 15px;">
	<c:if test="${notif}">
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
				Program ${nameNotif} berhasil ditambahkan.
			</div>
		</c:if>
	</div>
	<div class="col-lg-6" style="padding-right: 50px; padding-left: 15px;">
		
		<form name="frm" method="post" action="Tambah"
			enctype="multipart/form-data">
			<label><b>Nama</b></label>
			<div class="form-group" data-role="input-control">
				<input class="form-control" type="text" placeholder="nama program"
					name="nama" pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" required autofocus>
				<button class="btn-clear" tabindex="-1"></button>
			</div>

			<div class="col-lg-6" style="padding-left: 0px;">
				<label><b>Tanggal Mulai</b></label> <input class="form-control"
					id="datePicker" type="date" name="tanggalMulai" required />

				<script>
					$(function() {

						$("#datePicker").igDatePicker();

					});
				</script>

			</div>
			<div class="col-lg-6">
				<label><b>Tanggal Selesai</b></label> <input class="form-control"
					id="datePicker1" type="date" name="tanggalAkhir" required />

				<script>
					$(function() {

						$("#datePicker1").igDatePicker();

					});
				</script>
			</div>

			<label style="margin-top: 15px;"><b>Biaya</b></label>
			<div class="form-group" data-role="input-control">
				<input class="form-control" type="text" placeholder="masukan angka tanpa pemisah ribuan"
					name="biaya" pattern="^\d+$" required>
				<button class="btn-clear" tabindex="-1"></button>
			</div>
			<label><b>Donatur</b></label>
			<div class="form-group" data-role="input-control">
				<input class="form-control" type="text"
					placeholder="donatur program" name="donatur"
					pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]" required>
				<button class="btn-clear" tabindex="-1"></button>
			</div>

			<label><b>Ashnaf</b></label>
			<div class="form-group">
				<div class="col-lg-4">
					<label> <input type="checkbox" name="asnaf"
						value="fakirmiskin" /> <span class="check"></span> Fakir Miskin
					</label> <br /> <label> <input type="checkbox" name="asnaf"
						value="amil" /> <span class="check"></span> Amil
					</label> <br /> <label> <input type="checkbox" name="asnaf"
						value="muallaf" /> <span class="check"></span> Muallaf
					</label>
				</div>
				<div class="col-lg-4">
					<label> <input type="checkbox" name="asnaf" value="riqab" />
						<span class="check"></span> Riqab
					</label><br /> <label> <input type="checkbox" name="asnaf"
						value="gharim" /> <span class="check"></span> Gharim
					</label><br /> <label> <input type="checkbox" name="asnaf"
						value="fisabilillah" /> <span class="check"></span> Fisabilillah
					</label>
				</div>
				<div class="col-lg-4">
					<label> <input type="checkbox" name="asnaf"
						value="ibnusabil" /> <span class="check"></span> Ibnu Sabil
					</label>
				</div>
			</div>
	</div>
	<div class="col-lg-6">
		<label><b>Lokasi</b></label>
		<div class="form-group" data-role="input-control">
			<input class="form-control" type="text" placeholder="lokasi program"
				name="lokasi" pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]" required>
			<button class="btn-clear" tabindex="-1"></button>
		</div>
		<label><b>Tujuan</b></label>
		<div class="form-group" data-role="input-control">
			<input class="form-control" type="text" placeholder="tujuan program"
				name="tujuan" pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]" required>
			<button class="btn-clear" tabindex="-1"></button>
		</div>
		<label><b>Penanggung Jawab</b></label>
		<div class="form-group" data-role="input-control">
			<input class="form-control" type="text"
				placeholder="penanggungjawab program" name="penanggungjawab"
				pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]" required>
			<button class="btn-clear" tabindex="-1"></button>
		</div>
		<div class="col-lg-6" style="padding-left: 0px;">
			<label><b>Progres</b> (%)</label>
			<div class="form-group" data-role="input-control">
				<input class="form-control" type="text"
					placeholder="masukan angka tanpa presentase" name="progres" pattern="^\d+$"
					required>
				<button class="btn-clear" tabindex="-1"></button>
			</div>
		</div>
		<div class="col-lg-6" style="padding-left: 0px;">
			<label><b>Status</b></label>
			<div class="form-group">
				<select class="form-control" name="status">
					<option value="">--pilih status--</option>
					<option value="Belum Berjalan">Belum Berjalan</option>
					<option value="Sedang Berjalan">Sedang Berjalan</option>
					<option value="Selesai">Selesai</option>
				</select>
			</div>
		</div>
		<label><b>Foto</b> (optional)</label>

		<div class="form-group" data-role="input-control">
			<input class="form-control" placeholder="image.jpg" type="file"
				name="foto" value="" accept="image/*">
		</div>
		<button class="btn btn-lg btn-primary submit2" type="submit">Simpan</button>
		</form>
	</div>
</div>
<script src="Metro-UI-CSS-master/docs/js/hitua.js"></script>
<script src="./Metro UI CSS   Metro Bootstrap CSS Library_files/hit"></script>


<div class="metro notify-container"></div>

</html>