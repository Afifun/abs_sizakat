<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">
<meta name="description" content="Simple responsive css framework">
<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

</head>

<jsp:include page="header.jsp" flush="false" />
<body>

	<!-- NAVBAR
================================================== -->
	<!-- Docs master nav -->
	<!-- Fixed navbar -->



	<div class="container" style="padding-top: 60px">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><a href="Index">Home</a></li>				
				<c:choose>
				<c:when test="${currentSessionUser.isAdmin()}">
					<li><a href="ListAccount">Lihat Akun</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="ListProgramDonatur">Profile Saya</a></li>
				</c:otherwise>
				</c:choose>
				
				<li class="active">Edit Akun</li>
			</ol>
		</div>
	</div>
	<div class="container" style="margin-bottom:100px">
		<div class="col-lg-12">
			<h2>Edit Akun</h2>
			<c:choose>
			<c:when test="${currentSessionUser.isAdmin()}">
				<jsp:include page="formEditAccount.jsp" flush="false" />
			</c:when>
			<c:otherwise>
				<jsp:include page="formEditAccountDonatur.jsp" flush="false" />
			</c:otherwise>
			</c:choose>
			
			

		</div>
	</div>
		
<jsp:include page="footer.jsp" flush="false" />
</div>
</body>



<!-- Marketing messaging and featurettes
    ================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>
