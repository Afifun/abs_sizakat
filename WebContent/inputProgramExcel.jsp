<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">
<meta name="description" content="Simple responsive css framework">
<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">


</head>

<jsp:include page="header.jsp" flush="false" />

	<!-- NAVBAR
================================================== -->
	<!-- Docs master nav -->
	<!-- Fixed navbar -->

	<div class="container" style="padding-top: 60px">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><a href="Index">Home</a></li>
				<li class="active">Input Program via import file Excel</li>
			</ol>
		</div>
	</div>

	<div class="container">
		<div>
			<h2>Masukan file excel ke dalam form berikut</h2>
			<p>
				<a href="format.xlsx">Download Contoh Format File Excel</a>
			</p>
			<form action="Upload" method="post" enctype="multipart/form-data">
				<div class="form-group" data-role="input-control">
					<input class="form-control" type="file" name="excel" value=""
						required>
				</div>
				<button class="btn btn-lg btn-primary"
					style="margin-bottom: 20px; font-size: 12pt" type="submit">Upload</button>

				
						<div>
							<h2>${success_result}</h2>
						</div>
						<div>
						<c:forEach  var="item" items="${unsuccess_result}">
							<p style="color:red;">${ item }</p>
						</c:forEach>
						</div>
						
				<c:choose>
					<c:when test="${io_exception != null || invalid_format}">
						<div>
							<h2>Format/Jenis File Salah</h2>
							<p>
								Pastikan yang Anda upload adalah file <b>Excel</b>. yaitu file
								dengan ekstensi <b>.xls</b> atau <b>.xlsx</b>
							</p>
							<p>
								Pastikan pula format data dalam file <b>Excel</b> anda benar.
							</p>
						</div>
					</c:when>
					<c:when test="${sql_exception != null}">
						<div>
							<p>Terjadi kesalahan pada Database. Pastikan database yang Anda gunakan benar.</p>
						</div>
					</c:when>
				</c:choose>

			</form>
		</div>
	</div>

	<script src="Metro-UI-CSS-master/js/hitua.js"></script>


<div style="margin-top:200px">
	<jsp:include page="footer.jsp" flush="false" />
</div>
</html>
