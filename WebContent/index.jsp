<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">

</head>

<jsp:include page="header.jsp" flush="false" />
<!-- caraousel -->
    <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('assets/docs/slider/slider.png');"></div>
                <div class="carousel-caption">
                    <h3 style="color:#008dd0">Mari Bangun Kemitraan Bersama Kami</h3>
                    <p><i>
                    Fokus kami adalah mendayagunakan program rescue, rehabilitasi dan pemberdayaan untuk mengembangkan kemandirian.
                    
					</i>
                    </p>
                    <p><a href="ListProgram" class="btn btn-carousel">Lihat Semua Program</a></p>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('assets/docs/slider/slider2.png');"></div>
                <div class="carousel-caption">
                    <h3 style="color:#008dd0">"Bangun kepedulian, Bersinergi menebar peduli"</h3>
                    <p><i>
                    Terima kasih kepada para donatur/muzzaki yang bergabung untuk bersinergi menebar peduli.
                    Inilah bentuk transparansi kami untuk melaporkan kolaborasi kita. 
                    
						 </i>
                    </p>
                    <p><a href="ListProgram" class="btn btn-carousel">Lihat Semua Program</a></p>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('assets/docs/slider/slider3.png');"></div>
                <div class="carousel-caption">
                    <h3 style="color:#008dd0">Menggugah Nurani Menebar Peduli</h3>
                    <p><i>
                    Menggugah Nurani siapa saja, dimana saja dan kapan saja untuk Peduli pada nasib sesama dalam amal ibadah yang nyata, "karena yang terbaik diantara kita adalah yang paling besar kontribusinya terhadap sesama".
                    </i>
                    </p>
                    <p><a href="ListProgram" class="btn btn-carousel">Lihat Semua Program</a></p>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </div>
    <!-- end of carousel -->
    
    
        <div class="container2">
        <div class="container">                        
            <div class="row text-center">
                <div class="col-lg-4 col-sm-6">
                    <img class="logoSize" src="assets/docs/icon/donatur.png">
                    <h4><strong>Total Donatur</strong>
                        <p class="btn btn-logo">${account.size()} orang</p></h4>

                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <img class="logoSize" src="assets/docs/icon/danamasuk.png">
                        <h4><strong>Total Dana yang Masuk</strong>
                            <p class="btn btn-logo">Rp. 1.000.000.000,-</p></h4>

                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <img class="logoSize" src="assets/docs/icon/program.png">
                            <h4><strong>Total Program</strong>
                                <p class="btn btn-logo">${program.size()} program</p></h4>

                            </div>            
                        </div>

                    </div>
                    <!-- /.container -->        
                </div>
    <!-- /.section --
    <!-- /.container -->
    
    
    <div class="container">        

        <div class="row">
            <div class="col-lg-12">
                <h3>Program Terbaru</h3>
            </div>
        </div>
        <!-- /.row -->        
        <div class="row">
            <div class="col-lg-12">
                <div class="row text-center">
                	<c:forEach var="i" begin="0" end="3">
	                    <div class="col-lg-3 col-md-6">
	                        <div class="thumbnail">
	                        <c:choose>
	                        	<c:when test="${program.get(i).getFotoNama() != null}">
										<img class="imageProgramSize" src="${program.get(i).getFotoNama()}" alt="" >
								</c:when>
								<c:otherwise>
									<img class="imageProgramSize" src="images/not_available.jpg" alt="" >
								</c:otherwise>
							</c:choose>
	                            <div class="caption">
	                                <p class="home" style="border-radius: 50%;">${program.get(i).getTanggalBulan()}</p>
	                                <hr class="line">
	                                <h3>${program.get(i).getNama()}</h3>
	                                <p>${program.get(i).getTujuan()}</p>
	                                <p><a href="Detail?idDetail=${program.get(i).getId()}" class="btn btn-default moreDetail">Selengkapnya</a>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
                    </c:forEach>           
                </div>
                <div class="row">

                    <div>                        
                       <p><a href="ListProgram" class="btn btn-allprog">Semua Program &nbsp; &nbsp;<img class="nextarrow" src="assets/docs/icon/next.png"></img></a></p>
                    </div>
                </div>
            </div>                    
        </div>
        <!-- /.row -->            
    </div>

        <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
    
<jsp:include page="footer.jsp" flush="false" />
</html>
