<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SiZakat</title>
</head>

<jsp:include page="header.jsp" flush="false" />
<!-- CONTENT -->
<div class="container">
	<!-- JUDUL -->
	<div class="row" style="padding-top: 50pt;">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li class="active">Semua Program yang Anda Ikuti</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-10">
			<h2>Selamat Datang :</h2>
			<h4 class="well2"> ${akun.getNamaDonatur()} </h4>
		</div>
		<div class="col-lg-2">
			<form action="EditAccountDonatur" style="margin-bottom: 0px">
				<input class="button medium primary" type="hidden" name="idEdit"
					value="${akun.getId()}">
				<button class="btn btn-allprog" style="font-size: 13px;"
					type="submit">
					Edit Akun &nbsp;<img class="nextarrow"
						src="assets/docs/icon/edit.png"></img>
				</button>
			</form>
		</div>
	</div>


	<!-- ISI  -->
	<div class="row">
		<h2>Program yang Anda ikuti :</h2>
		
		<c:choose>
			<c:when test="${checkEmpty}">
				<div class="col-lg-10 text-left well2">
					<h4>Anda belum ikut dalam program apapun</h4>
				</div>
			</c:when>
			<c:otherwise>
				<div class="row text-center">
					<c:forEach var="item" items="${listProgramDonatur}">
						<div class="col-lg-3 col-md-6">
							<div class="thumbnail">
							<c:choose>
	                        	<c:when test="${program.get(i).getFotoNama() != null}">
										<img class="imageProgramSize" src="${program.get(i).getFotoNama()}" alt="" >
								</c:when>
								<c:otherwise>
									<img class="imageProgramSize" src="images/not_available.jpg" alt="" >
								</c:otherwise>
							</c:choose>
								<div class="caption">
									<p class="home" style="border-radius: 50%;">${item.getTanggalBulan()}</p>
									<hr class="line">
									<h3>${item.getNama()}</h3>
									<p>${item.getTujuan()}</p>
									<p>
										<a href="Detail?idDetail=${item.getId()}"
											class="btn btn-default moreDetail">Selengkapnya</a>
									</p>
								</div>
							</div>
						</div>
					</c:forEach>

					<%--For displaying Previous link except for the 1st page --%>

				</div>
				<div class="row text-right">
					<div class="col-lg-12">
						<ul class="pagination">
							<%--For displaying Previous link except for the 1st page --%>
							<c:if test="${currentPageD != 1}">
								<li><a href="program.do?page=${currentPageD - 1}">Previous</a>
								</li>
							</c:if>

							<%--For displaying Page numbers. 
					    The when condition does not display a link for the current page--%>

							<c:forEach begin="1" end="${noOfPagesD}" var="i">
								<c:choose>
									<c:when test="${currentPageD eq i}">
										<li><a href="#"
											style="background-color: #008dd0; color: black">${i}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="program.do?page=${i}">${i}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>


							<%--For displaying Next link --%>
							<c:if test="${currentPageD lt noOfPagesD}">
								<li><a href="program.do?page=${currentPageD + 1}">Next</a></li>
							</c:if>
						</ul>
					</div>
				</div>
			</c:otherwise>
		</c:choose>

	</div>

	<!-- END OF ISI -->
</div>

<c:choose>
<c:when test="${checkEmpty}">
	<div class="navbar-fixed-bottom" >
	<jsp:include page="footer.jsp" flush="false" />
	</div>
</c:when>
<c:otherwise>	
	<jsp:include page="footer.jsp" flush="false" />	
</c:otherwise>
</c:choose>



</html>