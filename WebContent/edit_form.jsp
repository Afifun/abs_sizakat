<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<!-- saved from url=(0033)http://metroui.org.ua/notify.html -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="product" content="Metro UI CSS Framework">
    <meta name="description" content="Simple responsive css framework">
    <meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

    <link href="Metro-UI-CSS-master/docs/css/metro-bootstrap.css" rel="stylesheet">
    <link href="Metro-UI-CSS-master/docs/css/metro-bootstrap-responsive.css" rel="stylesheet">
    <link href="Metro-UI-CSS-master/docs/css/iconFont.css" rel="stylesheet">
  
    <link href="Metro-UI-CSS-master/css/ganteng.css" rel="stylesheet">

    <!-- Load JavaScript Libraries -->
    <script src="Metro-UI-CSS-master/docs/js/jquery/jquery.min.js"></script>
    <script src="Metro-UI-CSS-master/docs/js/jquery/jquery.widget.min.js"></script>
    <script src="Metro-UI-CSS-master/docs/js/jquery/jquery.mousewheel.js"></script>
    <script src="Metro-UI-CSS-master/docs/js/prettify/prettify.js"></script>

    <!-- Metro UI CSS JavaScript plugins -->
    <script src="Metro-UI-CSS-master/docs/js/load-metro.js"></script>

    <!-- Local JavaScript -->
    <script src="Metro-UI-CSS-master/docs/js/docs.js"></script>
    <script src="Metro-UI-CSS-master/docs/js/github.info.js"></script>
<script>
	$(function() {
		$('#notify_btn_1').on('click', function() {
			$.Notify({
				shadow : true,
				position : 'bottom-right',
				content : "Metro UI CSS is awesome!!!"
			});
		});
		$('#notify_btn_2').on('click', function() {
			setTimeout(function() {
				$.Notify({
					style : {
						background : '#1ba1e2',
						color : 'white'
					},
					caption : 'Info...',
					content : "Metro UI CSS is Simple!!!"
				});
			}, 1000);
			setTimeout(function() {
				$.Notify({
					style : {
						background : 'red',
						color : 'white'
					},
					content : "Metro UI CSS is Sufficient!!!"
				});
			}, 2000);
			setTimeout(function() {
				$.Notify({
					style : {
						background : 'green',
						color : 'white'
					},
					content : "Metro UI CSS is Responsive!!!"
				});
			}, 3000);
			setTimeout(function() {
				$.Notify({
					content : "Default style for notify"
				});
			}, 4000);
		});
	});
</script>

<div class="row" style="margin-bottom: 50px">
	<div class="col-lg-12" style="padding-right: 50px; padding-left: 15px;">
	<c:if test="${notif}">
		<c:choose>	
		<c:when test="${nameNotif.equals('Error')}">
				<div class="alert alert-error">
					<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
					Program ${nama} gagal diperbarui.
				</div>
		</c:when>
		<c:otherwise>
			<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong>
					Program ${nama} berhasil diperbarui.
				</div>
		</c:otherwise>
		</c:choose>
	</c:if>
	</div>
	
	<div class="col-lg-6" style="padding-right: 50px; padding-left: 15px;">
		<form name="frm" method="post" action="Edit"
			enctype="multipart/form-data">
			<label>Nama</label>
			<div class="form-group" data-role="input-control">
				<input class="form-control" type="text" placeholder="nama program"
					name="nama" pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*"
					value="${program.nama}" required autofocus>
				<button class="btn-clear" tabindex="-1"></button>
			</div>
			<div class="col-lg-6" style="padding-left: 0px;">
				<label>Tanggal Mulai</label> <input class="form-control"
					id="datePicker" type="date" name="tanggalMulai"
					value=${program.tanggalMulai } required />

				<script>
					$(function() {

						$("#datePicker").igDatePicker();

					});
				</script>
			</div>
			<div class="col-lg-6">
				<label>Tanggal Selesai</label> <input class="form-control"
					id="datePicker1" type="date" name="tanggalAkhir"
					value=${program.tanggalAkhir } required />

				<script>
					$(function() {

						$("#datePicker1").igDatePicker();

					});
				</script>
			</div>

			<label style="margin-top: 15px;"><b>Biaya</b></label>
			<div class="form-group" data-role="input-control">
				<input class="form-control" type="text" placeholder="biaya program"
					name="biaya" pattern="^\d+$" value=${program.biaya } required>
				<button class="btn-clear" tabindex="-1"></button>
			</div>
			<label>Donatur</label>
			<div class="form-group" data-role="input-control">
				<input class="form-control" type="text"
					placeholder="donatur program" name="donatur"
					value="${program.getDonatur()}" required>
				<button class="btn-clear" tabindex="-1"></button>
			</div>
			<label>Ashnaf</label>
			<div class="form-group">
				<div class="col-lg-4">
					<label> <input type="checkbox" name="asnaf1"
						value="fakirmiskin"
						<c:if test="${asnaf[0] == 'fakirmiskin'}">
						checked = "checked"
				</c:if> />
						<span class="check"></span> Fakir Miskin
					</label> 
					<br /><label> <input type="checkbox" name="asnaf2" value="amil"
						<c:if test="${asnaf[1] == 'amil'}">
						checked = "checked"
				</c:if> />
						<span class="check"></span> Amil
					</label> <br /><label> <input type="checkbox" name="asnaf3"
						value="muallaf"
						<c:if test="${asnaf[2] == 'muallaf'}">
						checked = "checked"
				</c:if> />
						<span class="check"></span> Muallaf
					</label>
				</div>
				<div class="col-lg-4">
					<label> <input type="checkbox" name="asnaf4" value="riqab"
						<c:if test="${asnaf[3] == 'riqab'}">
						checked = "checked"
				</c:if> />
						<span class="check"></span> Riqab
					</label> <br /><label> <input type="checkbox" name="asnaf5"
						value="gharim"
						<c:if test="${asnaf[4] == 'gharim'}">
						checked = "checked"
				</c:if> />
						<span class="check"></span> Gharim
					</label><br /> <label> <input type="checkbox" name="asnaf6"
						value="fisabilillah"
						<c:if test="${asnaf[5] == 'fisabilillah'}">
						checked = "checked"
				</c:if> />
						<span class="check"></span> Fisabilillah
					</label>
				</div>
				<div class="col-lg-4">
					<label> <input type="checkbox" name="asnaf7"
						value="ibnusabil"
						<c:if test="${asnaf[6] == 'ibnusabil'}">
						checked = "checked"
				</c:if> />
						<span class="check"></span> Ibnu Sabil
					</label>
				</div>
			</div>
	</div>
	<div class="col-lg-6">
		<label>Lokasi</label>
		<div>
			<textarea class="form-control" placeholder="alamat lokasi program"
				style="width: 100%; height: 110px; margin-bottom:10px;" name="lokasi">${program.lokasi}</textarea>
		</div>
		<label>Tujuan</label>
		<div class="form-group" data-role="input-control">
			<input class="form-control" type="text" placeholder="tujuan program"
				name="tujuan" pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]"
				value="${program.tujuan}" required>
			<button class="btn-clear" tabindex="-1"></button>
		</div>
		<label>Penanggung Jawab</label>
		<div class="form-group" data-role="input-control">
			<input class="form-control" type="text"
				placeholder="penanggungjawab program" name="penanggungjawab"
				pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]"
				value="${program.penanggungjawab}" required>
			<button class="btn-clear" tabindex="-1"></button>
		</div>
		<div class="col-lg-6" style="padding-left: 0px;">
			<label>Progres (%)</label>
			<div class="form-group" data-role="input-control">
				<input class="form-control" type="text"
					placeholder="progres program" value="${program.progress}"
					name="progres" pattern="^\d+$" required>
				<button class="btn-clear" tabindex="-1"></button>
			</div>
		</div>
		<div class="col-lg-6" style="padding-left: 0px;">
			<label>Status</label>
			<div class="class="form-group">
				<select name="status" class="form-control">
					<option value="Belum Berjalan"
						<c:if test="${program.status == 'Belum Berjalan'}">
						selected = "selected"
						</c:if>>
						Belum Berjalan
					</option>
					<option value="Sedang Berjalan"
						<c:if test="${program.status == 'Sedang Berjalan'}">
						selected = "selected"
					    </c:if>>
					    Sedang Berjalan
				    </option>
					<option value="Selesai"
						<c:if test="${program.status  == 'Selesai'}">
						selected = "selected"
						</c:if>>
						Selesai
					</option>
				</select>
			</div>
			<input type="hidden" name="idEdit" value="${program.id}"> <br>
		</div>

		<label>Foto</label> <br> <img src="${program.fotoNama}"
			width="50%"> <br>
		<div class="form-group" data-role="input-control">
			<input class="form-control" type="file" name="foto" accept="image/*"
				value=${program.fotoNama}>
		</div>
		<button class="btn btn-lg btn-primary submit2"
			style="margin-bottom: 20px" type="submit">Simpan</button>
		<br>
	</div>
	</form>
</div>
