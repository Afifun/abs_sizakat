<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<title>SiZakat</title>
<link href="assets/docs/css/dataTables.bootstrap.css" rel="stylesheet">
</head>

<jsp:include page="header.jsp" flush="false" />

<div class="container" style="padding-bottom: 150px">
	<div class="row" style="padding-top: 50pt;">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li><a href="Index">Home</a></li>
				<li class="active">Lihat Akun</li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">

			<h2>Semua Donatur</h2>
			<div class="col-lg-12">
				<div class="panel-body well">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover"
							id="dataTables-example">
							<thead>
								<tr class="judulTable">
									<th style="text-align: center; vertical-align: middle;">Nama
										Donatur</th>
									<th style="text-align: center; vertical-align: middle;">Username</th>
									<th style="text-align: center; vertical-align: middle;">Tambahkan
										Program</th>
									<th style="text-align: center; vertical-align: middle;">Edit
										Akun</th>
									<th style="text-align: center; vertical-align: middle;">Delete
										Akun</th>
								</tr>
							</thead>

							<tbody>
								<c:forEach var="i" begin="0" end="${account.size()-1}">
									<tr>
										<!-- nama donatur -->
										<td>${account.get(i).getNamaDonatur()}</td>

										<!-- username -->
										<td>${account.get(i).getUsername()}</td>

										<!-- tambah program -->
										<td>
											
											<div style="max-height: 200px; width: 600px; overflow: auto;">
												<c:choose>
													<c:when													
														test="${!account.get(i).getListProgram().isEmpty()}">
														<c:forEach var="item"
															items="${account.get(i).getListProgram()}">
															<ul style="margin-left: -20px;">
																<li>${item.getNama()}</li>
															</ul>
														</c:forEach>
													</c:when>
													<c:otherwise>
														<i>Program ini belum memiliki donatur.</i>
													</c:otherwise>
												</c:choose>
											</div>
											<form action="TambahProgram" style="margin-bottom: 0px">
												<input class="button medium primary" type="hidden"
													name="idEdit" value="${account.get(i).getId()}">
												<button class="btn btn-allprog"
													style="font-size: 13px; padding: 2px" type="submit">
													<img class="nextarrow" src="assets/docs/icon/add.png"
														style="width: 10px"></img>
												</button>
											</form>
										</td>

										<!-- edit akun -->
										<td>
											<form action="EditAccount" style="margin-bottom: 0px">
												<input class="button medium primary" type="hidden"
													name="idEdit" value="${account.get(i).getId()}">
												<button class="btn btn-allprog" style="font-size: 13px;"
													type="submit">
													Edit Akun &nbsp;<img class="nextarrow"
														src="assets/docs/icon/edit.png"></img>
												</button>
											</form>
										</td>

										<!-- delete akun -->
										<td>
											<div class="modal fade"
												id="confirm-delete-${account.get(i).getId()}" tabindex="-1"
												role="dialog" aria-labelledby="myModalLabel"
												aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal"
																aria-hidden="true">&times;</button>
															<h4 class="modal-title" id="myModalLabel">Konfirmasi
																Delete</h4>

														</div>
														<div class="modal-body">
															<p>
																Apakah Anda yakin ingin menghapus akun dengan username <strong>${account.get(i).getUsername()}</strong>?
															</p>
															<p class="debug-url"></p>
														</div>

														<div class="modal-footer">
															<button type="button" class="btn btn-cancel danger"
																data-dismiss="modal">Cancel</button>
															<a href="DeleteAkun?idDelete=${account.get(i).getId()}"
																class="btn btn-danger danger">Delete</a>
														</div>
													</div>
												</div>
											</div> <a class="btn btn-allprog" style="font-size: 13px;"
											data-href="DeleteAkun?idDelete=${account.get(i).getId()}"
											data-toggle="modal"
											data-target="#confirm-delete-${account.get(i).getId()}"
											href="#">Delete Akun&nbsp;<img class="nextarrow"
												src="assets/docs/icon/delete.png"></img>
										</a><br>
										</td>

									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="footer.jsp" flush="false" />
<!-- Page-Level Plugin Scripts - Tables -->
<script src="assets/docs/js/jquery.dataTables.js"></script>
<script src="assets/docs/js/dataTables.bootstrap.js"></script>

<script>
			$(document).ready(
					function() {

						$('#dataTables-example').dataTable(
								{
									"iDisplayLength" : 5,									
									"aLengthMenu" : [ [ 5, 10, 25, 50, -1 ],
											[ 5, 10, 25, 50, "All" ] ]
								}).fnSort([ [ 0, 'asc' ] ]);

					});
		</script>


<script>
	$('#confirm-delete').on('show.bs.modal',function(e) {
				$(this).find('.danger').attr('href',
						$(e.relatedTarget).data('href'));

				$('.debug-url').html(
						'Delete URL: <strong>'
								+ $(this).find('.danger').attr('href')
								+ '</strong>');
			})
</script>
</html>
