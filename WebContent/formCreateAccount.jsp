<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<!-- saved from url=(0033)http://metroui.org.ua/notify.html -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="product" content="Metro UI CSS Framework">
<meta name="description" content="Simple responsive css framework">
<meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">



<!-- Load JavaScript Libraries -->
<script src="Metro-UI-CSS-master/docs/js/jquery/jquery.min.js"></script>
<script src="Metro-UI-CSS-master/docs/js/jquery/jquery.widget.min.js"></script>
<script src="Metro-UI-CSS-master/docs/js/jquery/jquery.mousewheel.js"></script>
<script src="Metro-UI-CSS-master/docs/js/prettify/prettify.js"></script>

<!-- Metro UI CSS JavaScript plugins -->
<script src="Metro-UI-CSS-master/docs/js/load-metro.js"></script>

<!-- Local JavaScript -->
<script src="Metro-UI-CSS-master/docs/js/docs.js"></script>
<script src="Metro-UI-CSS-master/docs/js/github.info.js"></script>

<title>SiZakat</title>
<script src="Metro-UI-CSS-master/docs/js/metro.min.js"></script>
</head>
<body>
	<c:if test="${notif}">
		<c:choose>
		<c:when test="${error}">
			<div class="alert alert-danger alert-error">			
				<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong>
				${nameNotif}.
			</div>
		</c:when>
		<c:otherwise>
			<div class="alert alert-success">			
				<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>${typeNotif}</strong>
				${nameNotif}.
			</div>
		</c:otherwise>
		</c:choose>		
	</c:if>
		
	<form style="width: 300px; margin-bottom: 50pt;" name="frm" method="post" action="TambahAkun"
		enctype="multipart/form-data">
		<label><b>Nama Donatur</b></label>
		<div class="form-group">
			<input class="form-control" type="text" placeholder="Nama Donatur" name="namaDonatur"
				pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" required autofocus>
		</div>
		<label><b>Username</b></label>
		<div class="form-group">
			<input class="form-control" type="text" placeholder="Username" name="username"
				pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" required autofocus>
		</div>
		<c:choose>
			<c:when test="${currentSessionUser.isAdmin()}">
			
			</c:when>
			<c:otherwise>
			
			</c:otherwise>
		</c:choose>
                    	
		<label><b>Password</b></label>
		<div class="form-group">
			<input class="form-control" type="password" name="password"
				placeholder="Password" required>
		</div>
		<label><b>Repeat Password</b></label>
		<div class="form-group">
			<input class="form-control" type="password" name="repeatPassword"
				placeholder="Repeat Password" required>
		</div>
		<button class="btn btn-lg btn-primary submit2" style="margin-top: 20px"
			type="submit">Simpan</button>

	</form>


	<script src="Metro-UI-CSS-master/docs/js/hitua.js"></script>
	<script src="./Metro UI CSS   Metro Bootstrap CSS Library_files/hit"></script>


	<div class="metro notify-container"></div>
</body>
</html>