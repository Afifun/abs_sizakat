<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


<!-- Load JavaScript Libraries -->
<script src="assets/docs/js/jquery/jquery.min.js"></script>
<script src="assets/docs/js/jquery/jquery.widget.min.js"></script>
<script src="assets/docs/js/jquery/jquery.mousewheel.js"></script>
<script src="assets/docs/js/jquery/jquery.dataTables.js"></script>
<script src="assets/docs/js/prettify/prettify.js"></script>

<!-- Metro UI CSS JavaScript plugins -->
<script src="assets/docs/js/load-metro.js"></script>

<!-- Local JavaScript -->
<script src="assets/docs/js/github.info.js"></script>

<title>SiZakat</title>
</head>
<body class="metro">

	<!-- NAVBAR
================================================== -->
	<!-- Docs master nav -->
	<!-- Fixed navbar -->
	<jsp:include page="header.jsp" flush="false" />
	<div class="container" style="margin-top: 120px">
		<div class="row text-center">
			<div class="col-lg-12">
			
				<form action="Login" method="post"
					class="bootstrap-admin-login-form" style="margin-top: 50px">
					<h1>Login</h1>
					<%
					String hasil = "";

					if (true) {
						hasil = "password/username tidak cocok";
					}
				%>
					<label><%=request.getAttribute("respon")%></label>
					<div class="form-group">
						<input class="form-control" type="text" placeholder="Username"
							name="username" pattern="[a-zA-Z]*[/-.',]*[a-zA-Z]*" required
							autofocus>
					</div>
					<div class="form-group">
						<input class="form-control" type="password" name="password"
							placeholder="Password" required>
					</div>
					<button type="submit" class="btn btn-lg btn-primary submit2"
						>Login</button>
				</form>

			</div>
		</div>
	</div>

	</div>
</body>
</html>
