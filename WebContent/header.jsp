<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE head PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- <link href="assets/docs/css/metro-bootstrap.css" rel="stylesheet">
<link href="assets/docs/css/metro-bootstrap-responsive.css" rel="stylesheet">
<link href="assets/docs/css/iconFont.css" rel="stylesheet">
<link href="assets/docs/css/ganteng.css" rel="stylesheet"> -->

<!-- Load JavaScript Libraries -->
<script src="assets/docs/js/jquery/jquery.min.js"></script>
<script src="assets/docs/js/jquery/jquery.widget.min.js"></script>
<script src="assets/docs/js/jquery/jquery.mousewheel.js"></script>
<script src="assets/docs/js/jquery/jquery.dataTables.js"></script>
<script src="assets/docs/js/prettify/prettify.js"></script>

<script src="assets/docs/js/jquery-1.10.2.js"></script>
<script src="assets/docs/js/bootstrap.js"></script>

<!-- Metro UI CSS JavaScript plugins -->
<script src="assets/docs/js/load-metro.js"></script>

<!-- Local JavaScript -->
<script src="assets/docs/js/github.info.js"></script>
<script src="assets/docs/js/jquery-1.10.2.js"></script>

<!-- Bootstrap core CSS -->
<link href="assets/docs/css/bootstrap.css" rel="stylesheet">
<link href="assets/docs/css/bootstrap.min.css" rel="stylesheet">

<!-- Add custom CSS -->    
<link href="assets/docs/css/bluetemplate.css" rel="stylesheet">

<head>
<title>SiZakat</title>
<!-- <link rel="shortcut icon" href="http://sstatic.net/stackoverflow/img/favicon.ico"> -->
</head>


<body>
<!-- HEADER -->
    <nav class="navbar navbar-fixed-top navbar-inverse2" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>                      
                        <c:choose>
						<c:when test="${currentSessionUser != null && currentSessionUser.isAdmin()}">
							<a class="navbar-brand" href="Index"><img class="logo" style="width: 150px;" src="assets/docs/icon/logoadmin.png" alt="home"></a>
						</c:when>
						<c:otherwise>
							<a class="navbar-brand" href="Index"><img class="logo" src="assets/docs/icon/logo.png" alt="home"></a>
						</c:otherwise>
						</c:choose>		               
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <div class="forceRight">
                    <ul class="nav navbar-nav">
                    	<c:choose>
                    	<c:when test="${currentSessionUser.isAdmin()}">
	                    	<li><a href="ListProgram">Program</a>
	                        </li>
                    	</c:when>
                    	<c:otherwise>
	                    	<li><a href="ListProgram">Program</a>
	                        </li>
                    	</c:otherwise>
                    	</c:choose>
                    	                                           
                        <c:if test="${currentSessionUser != null && currentSessionUser.isAdmin()}">
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Input Program <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="Tambah">Isi Form</a>
                            </li>
                            <li><a href="inputProgramExcel.jsp">Import Excel</a>
                            </li>                            
                        </ul>
                    </li>
                    <li><a href="ListAccount">Lihat Akun</a></li>
					<li><a href="TambahAkun">Buat Akun</a></li>
                    </c:if>
                    
                    <c:if test="${currentSessionUser != null && !currentSessionUser.isAdmin()}">                                            
					<li><a href="ListProgramDonatur">Profile Saya</a></li>
                    </c:if>
                                                
                        <li><a href="kontakKami.jsp">Kontak Kami</a>
                        </li>
                        <c:choose>
						<c:when test="${currentSessionUser != null}">
							<li><a href="Logout">Logout</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="Login">Login</a></li>
						</c:otherwise>
						</c:choose>
									                        
                    </ul>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- END OF HEADER -->
</body>
</html>

<!--  -->