<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">

<!-- Core CSS - Include with every page -->
<link href="assets/docs/css/dataTables.bootstrap.css" rel="stylesheet">

</head>

<div class="panel-body" style="padding-right: 100px">
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover"
			id="dataTables-example">
			<thead>
				<tr>
					<th>Nama Program</th>
					<th>Status</th>
					<th>List Donatur</th>
					<th>Edit</th>
					<th>Delete</th>
					<th>Detail</th>
				</tr>
			</thead>

			<tbody>
				<c:forEach items="${program}" var="program">
					<tr>
						<!-- program -->
						<td>${program.nama}</td>

						<!-- status -->
						<td><c:if test="${program.status == 'Belum Berjalan'}">
						Belum Berjalan
						</c:if> <c:if test="${program.status == 'Sedang Berjalan'}">
						Sedang Berjalan
							</c:if> <c:if test="${program.status  == 'Selesai'}">
						Selesai
							</c:if></td>

						<!-- list donatur -->
						<td>${program.donatur}</td>

						<!-- edit detail -->
						<td><form action="DetailEdit" style="margin-bottom: 0px">
								<input class="button medium primary" type="hidden" name="idEdit"
									value="${program.id}">
								<button style="width: 65px" class="button medium primary"
									type="submit">Edit</button>
							</form></td>
						
						<!-- delete -->
						<td>
							<form id="dlt" action="Delete" style="margin-bottom: 0px">
								<input class="button medium primary" type="hidden"
									name="idDelete" value="${program.id}">

							</form>
							<button style="width: 65px" class="button medium primary"
								onclick="myFunction(${program.id})">Delete</button>
						</td>
						
						<!-- detail -->
						<td><form style="margin-bottom: 0px" action="Detail">
								<input class="button medium primary" type="hidden" name="idEdit"
									value="${program.id}">
								<button style="width: 65px" class="button medium primary"
									type="submit">Detail</button>
							</form></td>
					</tr>
				</c:forEach>
			</tbody>

		</table>
	</div>
</div>


<!-- Page-Level Plugin Scripts - Tables --> 
 <script src="assets/docs/js/jquery.dataTables.js"></script>
 <script src="assets/docs/js/dataTables.bootstrap.js"></script>


<script>
function myFunction(y) {
    if (confirm("Apakah Anda yakin untuk menghapus program?") == true) {
    	document.location.href="Delete?idDelete=" + y; 
    } else {
    }
}
</script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable().fnSort( [ [0,'desc'] ] );        
    });
</script>
</html>